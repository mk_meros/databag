﻿namespace DataBag.EntityFramework.Helpers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class TypeHelper
    {
        public static IEnumerable<PropertyInfo> GetProperties<TAttribute>(this Type type)
            where TAttribute : Attribute
        {
            return type
                .GetProperties()
                .Where(p => p.CustomAttributes
                             .Any(a => a.AttributeType == typeof(TAttribute)));
        }

        public static void Add(this IEnumerable collection, object obj)
        {
            collection.GetType().GetMethod("Add").Invoke(collection, new object[] { obj });
        }

        public static void Remove(this IEnumerable collection, object obj)
        {
            collection.GetType().GetMethod("Remove").Invoke(collection, new object[] { obj });
        }

        public static object FirstOrDefault(this IEnumerable collection, Dictionary<string, object> properties)
        {
            foreach (var item in collection)
            {
                if (item.HasProperties(properties))
                {
                    return item;
                }
            }

            return null;
        }

        public static bool HasProperties(this object obj, Dictionary<string, object> values)
        {
            foreach (var property in values)
            {
                var value = obj.GetProperty(property.Key);
                if (!value.IsEqualTo(property.Value))
                {
                    return false;
                }
            }

            return true;
        }

        public static object GetProperty(this object obj, string name)
        {
            return obj.GetType().GetProperty(name).GetValue(obj);
        }

        public static bool IsEqualTo(this object obj1, object obj2)
        {
            return obj1 == obj2 || obj1.Equals(obj2);
        }
    }
}
