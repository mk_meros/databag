﻿namespace DataBag.EntityFramework
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Reflection;

    using DataBag.Core;
    using DataBag.EntityFramework.Attributes;
    using Helpers;

    public class DataBagDbContext
    {
        private List<DbContext> dbContexts;

        public DataBagDbContext(IDataBag dataBag, params DbContext[] dbContexts)
        {
            this.DataBag = dataBag;
            this.dbContexts = dbContexts.ToList();
        }

        public IDataBag DataBag { get; private set; }

        /// <summary>
        /// Add DB context.
        /// </summary>
        /// <param name="dbContext">The DB context.</param>
        /// <returns>The Data Bag Context.</returns>
        public DataBagDbContext AddDbContext(DbContext dbContext)
        {
            dbContexts.Add(dbContext);
            return this;
        }

        /// <summary>
        /// Find a record by using values of registered variables associated with properties marked by [FindBy] attribute.
        /// </summary>
        /// <typeparam name="T">type of object.</typeparam>
        /// <returns>The matching object or null.</returns>
        public T Find<T>()
            where T : class, new()
        {
            return (T)Find(typeof(T));
        }

        /// <summary>
        /// Find a record by using values of registered variables associated with properties marked by [FindBy] attribute.
        /// </summary>
        /// <param name="type">type of object.</param>
        /// <returns>The matching object or null.</returns>
        public object Find(Type type)
        {
            var where = GetWhere(type);

            var dbContext = GetDbContext(type);
            var dbSet = GetDbSet(dbContext, type);
            return dbSet.FirstOrDefault(where);
        }

        /// <summary>
        /// Add new record by using values from registered variables.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        /// <returns>Added object.</returns>
        public T Add<T>(bool saveDbContext = true)
            where T : class, new()
        {
            return (T)Add(typeof(T), saveDbContext);
        }

        /// <summary>
        /// Add new record by using values from registered variables.
        /// </summary>
        /// <param name="type">Type of object.</param>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        /// <returns>Added object.</returns>
        public object Add(Type type, bool saveDbContext = true)
        {
            var instance = DataBag.GetFor(type);
            UpdateForeignKeys(instance);

            var dbContext = GetDbContext(type);
            var dbSet = GetDbSet(dbContext, type);
            dbSet.Add(instance);

            if (saveDbContext)
            {
                dbContext.SaveChanges();
            }

            return instance;
        }

        /// <summary>
        /// Remove a record found by values of registered variables associated with properties marked by [FindBy] attribute.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        public void Remove<T>(bool saveDbContext = true)
            where T : class, new()
        {
            Remove(typeof(T), saveDbContext);
        }

        /// <summary>
        /// Remove a record found by values of registered variables associated with properties marked by[FindBy] attribute.
        /// </summary>
        /// <param name="type">Type of object.</param>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        public void Remove(Type type, bool saveDbContext = true)
        {
            var instance = Find(type);

            var dbContext = GetDbContext(type);
            var dbSet = GetDbSet(dbContext, type);
            dbSet.Remove(instance);

            if (saveDbContext)
            {
                dbContext.SaveChanges();
            }
        }

        /// <summary>
        /// Remove the given record.
        /// </summary>
        /// <param name="instance">The record to be removed.</param>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        public void Remove(object instance, bool saveDbContext = true)
        {
            var type = instance.GetType();

            var dbContext = GetDbContext(type);
            var dbSet = GetDbSet(dbContext, type);
            dbSet.Remove(instance);

            if (saveDbContext)
            {
                dbContext.SaveChanges();
            }
        }

        /// <summary>
        /// Automatically search and register new variables for properties marked with [DBVariable] attribute.
        /// The search will be performed in types used in included DB Context objects.
        /// </summary>
        /// <returns></returns>
        public DataBagDbContext RegisterDBVariables()
        {
            var dbContextTypes = GetDBContextTypes();
            var dbVariables = dbContextTypes
                .SelectMany(t => t.GetProperties())
                .Where(p => p.GetCustomAttribute<DBVariableAttribute>() != null);

            foreach (var dbVariable in dbVariables)
            {
                if (ShouldRegisterDBVariable(dbVariable))
                {
                    RegisterDBVariable(dbVariable);
                }
            }

            return this;
        }

        private Dictionary<string, object> GetWhere(Type type)
        {
            var where = new Dictionary<string, object>();

            var findByProperties = type.GetProperties<FindByAttribute>();
            foreach (var property in findByProperties)
            {
                var value = GetValueFor(property);
                where.Add(property.Name, value);
            }

            return where;
        }

        private void UpdateForeignKeys(object instance)
        {
            var foreignKeys = GetForeignKeys(instance.GetType());
            foreach (var property in foreignKeys)
            {
                var value = GetValueFor(property);
                property.SetValue(instance, value);
            }
        }

        private object GetValueFor(PropertyInfo property)
        {
            if (DataBag.IsRegistered(property))
            {
                return DataBag.Get(property);
            }

            var foreignKey = property.GetCustomAttribute<CustomForeignKeyAttribute>();
            if (foreignKey != null)
            {
                if (DataBag.IsRegistered(foreignKey))
                {
                    return DataBag.Get(foreignKey);
                }

                var classInstance = Find(foreignKey.Class);
                if (classInstance != null)
                {
                    return classInstance.GetProperty(foreignKey.Name);
                }
            }
            else if (IsForeignKey(property))
            {
                return Find(property.PropertyType);
            }

            throw new DataBagEFException($"Cannot find value for '{property.Name}' in type '{property.DeclaringType.Name}'");
        }

        private bool ShouldRegisterDBVariable(PropertyInfo dbVariable)
        {
            if (!DataBag.IsRegistered(dbVariable))
            {
                return true;
            }

            var info = dbVariable.GetCustomAttribute<DBVariableAttribute>();
            return info.OverrideIfAlreadyRegistered;
        }

        private void RegisterDBVariable(PropertyInfo dbVariable)
        {
            var name = dbVariable.ToVariableName();
            DataBag.RegisterDynamic(
                name,
                () =>
                {
                    var instance = Find(dbVariable.DeclaringType);
                    if (instance == null)
                    {
                        throw new DataBagEFException($"Cannot find existing record with value for DBVariable '{name}'");
                    }

                    return instance.GetProperty(dbVariable.Name);
                });
        }

        private IEnumerable<PropertyInfo> GetForeignKeys(Type type)
        {
            var foreignKeys = type.GetProperties().Where(IsForeignKey);
            var customForeignKeys = type.GetProperties<CustomForeignKeyAttribute>();

            return foreignKeys.Concat(customForeignKeys).Distinct();
        }

        private bool IsForeignKey(PropertyInfo propertyInfo)
        {
            return !propertyInfo.PropertyType.IsGenericType &&
                    propertyInfo.GetAccessors().Any(a => a.IsVirtual);
        }

        private DbContext GetDbContext(Type type)
        {
            try
            {
                return dbContexts.First(c => GetDbSet(c, type) != null);
            }
            catch (Exception)
            {
                throw new DataBagEFException($"Cannot find DbContext that contains type '{type.Name}'");
            }
        }

        private IEnumerable GetDbSet(DbContext dbContext, Type type)
        {
            var dbSet = dbContext
                .GetType()
                .GetProperties()
                .FirstOrDefault(p => p.PropertyType.IsGenericType &&
                            p.PropertyType.GetGenericArguments().Contains(type));

            if (dbSet != null)
            {
                return (IEnumerable)dbSet.GetValue(dbContext);
            }

            return null;
        }

        private IEnumerable<Type> GetDBContextTypes()
        {
            return dbContexts
                .SelectMany(c => c.GetType().GetProperties())
                .Where(p => p.PropertyType.IsGenericType)
                .Select(p => p.PropertyType.GetGenericArguments()[0])
                .Distinct();
        }
    }
}
