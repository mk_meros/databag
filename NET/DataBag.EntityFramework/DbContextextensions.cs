﻿namespace DataBag.EntityFramework
{
    using System;
    using System.Data.Entity;
    using DataBag.Core;

    public static class DbContextExtensions
    {
        /// <summary>
        /// Add new record by using values from registered variables.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="dbContext">The DB Context.</param>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        /// <returns>Added object.</returns>
        public static T Add<T>(this DbContext dbContext, IDataBag dataBag, bool saveDbContext = true)
            where T : class, new()
        {
            return dbContext.GetFor(dataBag).Add<T>(saveDbContext);
        }

        /// <summary>
        /// Add new record by using values from registered variables.
        /// </summary>
        /// <param name="dbContext">The DB Context.</param>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="type">Type of object.</param>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        /// <returns>Added object.</returns>
        public static object Add(this DbContext dbContext, IDataBag dataBag, Type type, bool saveDbContext = true)
        {
            return dbContext.GetFor(dataBag).Add(type, saveDbContext);
        }

        /// <summary>
        /// Remove a record found by values of registered variables associated with properties marked by [FindBy] attribute.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="dbContext">The DB Context.</param>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        public static void Remove<T>(this DbContext dbContext, IDataBag dataBag, bool saveDbContext = true)
            where T : class, new()
        {
            dbContext.GetFor(dataBag).Remove<T>(saveDbContext);
        }

        /// <summary>
        /// Remove a record found by values of registered variables associated with properties marked by [FindBy] attribute.
        /// </summary>
        /// <param name="dbContext">The DB Context.</param>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="type">Type of object.</param>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        public static void Remove(this DbContext dbContext, IDataBag dataBag, Type type, bool saveDbContext = true)
        {
            dbContext.GetFor(dataBag).Remove(type, saveDbContext);
        }

        /// <summary>
        /// Remove the given record.
        /// </summary>
        /// <param name="dbContext">The DB Context.</param>
        /// <param name="instance">The record to be removed.</param>
        /// <param name="saveDbContext">Save changes in DB Context.</param>
        public static void Remove(this DbContext dbContext, object instance, bool saveDbContext = true)
        {
            dbContext.GetFor(null).Remove(instance, saveDbContext);
        }

        /// <summary>
        /// Find a record by using values of registered variables associated with properties marked by [FindBy] attribute.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="dbContext">The DB Context.</param>
        /// <param name="dataBag">The data bag.</param>
        /// <returns>The matching object or null.</returns>
        public static T Find<T>(this DbContext dbContext, IDataBag dataBag)
            where T : class, new()
        {
            return dbContext.GetFor(dataBag).Find<T>();
        }

        /// <summary>
        /// Find a record by using values of registered variables associated with properties marked by [FindBy] attribute.
        /// </summary>
        /// <param name="dbContext">The DB Context.</param>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="type">Type of object.</param>
        /// <returns>The matching object or null.</returns>
        public static object Find(this DbContext dbContext, IDataBag dataBag, Type type)
        {
            return dbContext.GetFor(dataBag).Find(type);
        }

        private static DataBagDbContext GetFor(this DbContext dbContext, IDataBag dataBag)
        {
            return new DataBagDbContext(dataBag, dbContext);
        }
    }
}
