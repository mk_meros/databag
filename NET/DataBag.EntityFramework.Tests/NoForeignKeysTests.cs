﻿namespace DataBag.EntityFramework.Tests
{
    using DataBag.Core;
    using DataBag.EntityFramework.Tests.DataModel;
    using NUnit.Framework;

    [TestFixture]
    public class NoForeignKeysTests : BaseTest
    {
        [Test]
        public void AddEntity()
        {
            var dataBag = GetDataBag();
            var context = new BloggingContext();

            var addedBlog = context.Add<Blog>(dataBag);

            Assert.That(addedBlog, Is.Not.Null);
            Assert.That(addedBlog.BlogId, Is.GreaterThan(0));
            Assert.That(addedBlog.Name, Is.EqualTo(dataBag.Get<Blog>(b => b.Name)));
            Assert.That(addedBlog.Url, Is.EqualTo(dataBag.Get<Blog>(b => b.Url)));
            Assert.That(addedBlog.Posts.Count, Is.EqualTo(0));
        }

        [Test]
        public void RemoveEntity()
        {
            var dataBag = GetDataBag();
            var context = new BloggingContext();

            context.Add<Blog>(dataBag);
            context.Remove<Blog>(dataBag);

            var existingBlog = context.Find<Blog>(dataBag);
            Assert.That(existingBlog, Is.Null);
        }

        [Test]
        public void RemoveInstance()
        {
            var dataBag = GetDataBag();
            var context = new BloggingContext();

            var addedBlog = context.Add<Blog>(dataBag);
            context.Remove(addedBlog);

            var existingBlog = context.Find<Blog>(dataBag);
            Assert.That(existingBlog, Is.Null);
        }

        [Test]
        public void FindRecord()
        {
            var dataBag = GetDataBag();
            var context = new BloggingContext();

            context.Add<Blog>(dataBag);
            var existingRecord = context.Find<Blog>(dataBag);

            Assert.That(existingRecord, Is.Not.Null);
            Assert.That(existingRecord.BlogId, Is.GreaterThan(0));
            Assert.That(existingRecord.Name, Is.EqualTo(dataBag.Get<Blog>(b => b.Name)));
            Assert.That(existingRecord.Url, Is.EqualTo(dataBag.Get<Blog>(b => b.Url)));
            Assert.That(existingRecord.Posts.Count, Is.EqualTo(0));
        }

        [Test]
        public void FindNoRecords()
        {
            var dataBag = GetDataBag();
            var context = new BloggingContext();

            var existingBlog = context.Find<Blog>(dataBag);
            Assert.That(existingBlog, Is.Null);
        }

        [Test]
        public void FindNoMatchingRecord()
        {
            var dataBag = GetDataBag();
            var context = new BloggingContext();
            context.Add<Blog>(dataBag);

            using (dataBag
                .ForTempChanges()
                .Register<Blog>(b => b.Name, "New name"))
            {
                var existingBlog = context.Find<Blog>(dataBag);
                Assert.That(existingBlog, Is.Null);
            }
        }

        [Test]
        public void FindFromMultipleRecords()
        {
            var dataBag = GetDataBag();
            var context = new BloggingContext();

            using (dataBag.ForTempChanges())
            {
                for (int i = 0; i < 10; i++)
                {
                    dataBag.Register<Blog>(b => b.Name, $"Name {i}");
                    dataBag.Register<Blog>(b => b.Url, $"URL {i}");
                    context.Add<Blog>(dataBag);
                }
            }

            using (dataBag.ForTempChanges()
                .Register<Blog>(b => b.Name, $"Name 5"))
            {
                var existingBlog = context.Find<Blog>(dataBag);
                Assert.That(existingBlog, Is.Not.Null);
                Assert.That(existingBlog.Name, Is.EqualTo("Name 5"));
                Assert.That(existingBlog.Url, Is.EqualTo("URL 5"));
            }
        }
    }
}
