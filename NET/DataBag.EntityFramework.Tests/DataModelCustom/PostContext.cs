﻿namespace DataBag.EntityFramework.Tests.DataModelCustom
{
    using System.Data.Entity;

    public partial class PostContext : DbContext
    {
        public PostContext()
            : base("name=BloggingContext")
        {
        }
        
        public virtual DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
