﻿namespace DataBag.EntityFramework.Tests.DataModelCustom
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using DataBag.Core;
    using DataBag.Core.Variables.Attributes;
    using DataBag.EntityFramework.Attributes;

    public partial class Post : IRegisterVariables
    {
        [DBVariable]
        [IgnoreWhenResolvingClass]
        public int PostId { get; set; }

        [FindBy]
        [StringLength(200)]
        public string Title { get; set; }

        [Column(TypeName = "ntext")]
        public string Content { get; set; }
        
        [CustomForeignKey(typeof(Blog), nameof(Blog.BlogId))]
        public int BlogId { get; set; }

        public void Register(IDataBag dataBag)
        {
            dataBag.Register<Post>(p => p.Title, "Post title")
                   .Register<Post>(p => p.Content, "Post content");
        }
    }
}
