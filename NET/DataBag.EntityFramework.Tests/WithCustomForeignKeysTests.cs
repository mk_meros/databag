﻿namespace DataBag.EntityFramework.Tests
{
    using System.Data.Entity.Infrastructure;
    using System.Linq;

    using DataBag.Core;
    using DataBag.EntityFramework.Tests.DataModelCustom;
    using NUnit.Framework;

    [TestFixture]
    public class WithCustomForeignKeysTests : BaseTest
    {
        [Test]
        public void CannotFindType()
        {
            var dbContext = new DataBagDbContext(
                GetDataBag(),
                new BlogContext());

            Assert.Throws<DataBagEFException>(() =>
            {
                dbContext.Add<WithCustomForeignKeysTests>();
            });
        }

        [Test]
        public void AddEntity()
        {
            var dbContext = new DataBagDbContext(
                GetDataBag(),
                new BlogContext(),
                new PostContext());

            dbContext.Add<Blog>();
            var addedPost = dbContext.Add<Post>();

            Assert.That(addedPost, Is.Not.Null);
            Assert.That(addedPost.PostId, Is.GreaterThan(0));
            Assert.That(addedPost.Title, Is.EqualTo(dbContext.DataBag.Get<Post>(p => p.Title)));
            Assert.That(addedPost.Content, Is.EqualTo(dbContext.DataBag.Get<Post>(p => p.Content)));
            Assert.That(addedPost.BlogId, Is.GreaterThan(0));
        }

        [Test]
        public void DBVariables()
        {
            var db = new DataBagDbContext(
                GetDataBag(),
                new BlogContext(),
                new PostContext());

            Assert.That(db.DataBag.IsRegistered<Blog>(b => b.BlogId), Is.False);
            Assert.That(db.DataBag.IsRegistered<Post>(p => p.PostId), Is.False);

            db.RegisterDBVariables();

            Assert.That(db.DataBag.IsRegistered<Blog>(b => b.BlogId), Is.True);
            Assert.That(db.DataBag.IsRegistered<Post>(p => p.PostId), Is.True);

            Assert.Throws<DataBagEFException>(() =>
            {
                db.DataBag.Get<Blog>(b => b.BlogId);
            });

            Assert.Throws<DataBagEFException>(() =>
            {
                db.DataBag.Get<Post>(p => p.PostId);
            });

            var addedBlog = db.Add<Blog>();
            var addedPost = db.Add<Post>();

            Assert.That(db.DataBag.Get<Blog>(b => b.BlogId), Is.EqualTo(addedBlog.BlogId));
            Assert.That(db.DataBag.Get<Post>(p => p.PostId), Is.EqualTo(addedPost.PostId));
        }

        [Test]
        public void AddEntityCannotFindForeignKey()
        {
            var dbContext = new DataBagDbContext(
                GetDataBag(),
                new BlogContext(),
                new PostContext());

            Assert.Throws<DataBagEFException>(() =>
            {
                dbContext.Add<Post>();
            });         
        }

        [Test]
        public void AddEntityCannotFindForeignKeyWithCustomId()
        {
            var dbContext = new DataBagDbContext(
                GetDataBag(),
                new BlogContext(),
                new PostContext());

            dbContext.DataBag.Register<Post>(p => p.BlogId, -1);

            Assert.Throws<DbUpdateException>(() =>
            {
                dbContext.Add<Post>();
            });
        }

        [Test]
        public void AddMultipleRecords()
        {
            var blogContext = new BlogContext();
            var postContext = new PostContext();
            var dbContext = new DataBagDbContext(GetDataBag())
                .AddDbContext(blogContext)
                .AddDbContext(postContext);

            using (dbContext.DataBag.ForTempChanges())
            {
                for (int i = 1; i <= 10; i++)
                {
                    dbContext.DataBag.Register<Blog>(b => b.Name, $"Name {i}");
                    dbContext.DataBag.Register<Blog>(b => b.Url, $"URL {i}");
                    dbContext.Add<Blog>();

                    for (int j = 1; j <= 2; j++)
                    {
                        dbContext.DataBag.Register<Post>(p => p.Title, $"Title {i} {j}");
                        dbContext.DataBag.Register<Post>(p => p.Content, $"Content {i} {j}");
                        dbContext.Add<Post>();
                    }        
                }
            }

            Assert.That(blogContext.Blogs.Count(), Is.EqualTo(10));
            Assert.That(postContext.Posts.Count(), Is.EqualTo(20));

            var blog = blogContext.Blogs.First(b => b.Name == "Name 5");
            var post = postContext.Posts.First(p => p.Title == "Title 5 1");
            Assert.That(blog.Url, Is.EqualTo("URL 5"));
            Assert.That(blog.BlogId, Is.EqualTo(post.BlogId));
        }
    }
}
