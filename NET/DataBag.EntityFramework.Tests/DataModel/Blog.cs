﻿namespace DataBag.EntityFramework.Tests.DataModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using DataBag.Core;
    using DataBag.EntityFramework;

    public partial class Blog : IRegisterVariables
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Justification = "Auto generated code")]
        public Blog()
        {
            Posts = new HashSet<Post>();
        }

        public int BlogId { get; set; }

        [FindBy]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Url { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Auto generated code")]
        public virtual ICollection<Post> Posts { get; set; }

        public void Register(IDataBag dataBag)
        {
            dataBag.Register<Blog>(b => b.Name, "Blog name")
                   .Register<Blog>(b => b.Url, "http://blog.com");
        }
    }
}
