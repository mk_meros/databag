﻿namespace DataBag.EntityFramework.Tests.DataModel
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using DataBag.Core;

    public partial class Post : IRegisterVariables
    {
        public int PostId { get; set; }

        [FindBy]
        [StringLength(200)]
        public string Title { get; set; }

        [Column(TypeName = "ntext")]
        public string Content { get; set; }
        
        public int BlogId { get; set; }

        public virtual Blog Blog { get; set; }

        public void Register(IDataBag dataBag)
        {
            dataBag.Register<Post>(p => p.Title, "Post title")
                   .Register<Post>(p => p.Content, "Post content");
        }
    }
}
