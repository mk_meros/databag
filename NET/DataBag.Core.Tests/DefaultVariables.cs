﻿namespace DataBag.Core.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using NUnit.Framework.Internal;

    [TestFixture]
    public class DefaultVariables
    {
        [Test]
        public void GetNames()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            Assert.That(bag.GetNames().Contains("null"));
            Assert.That(bag.GetNames().Contains("guid"));
        }

        [Test]
        public void VariableNameIsCaseInsensitive()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            Assert.That(bag.IsRegistered("gUiD"), Is.True);
            Assert.That(bag.Resolve("[GuId]"), Is.TypeOf(typeof(Guid)));
        }

        [Test]
        public void Common()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            Assert.That(bag.Resolve("[null]"), Is.Null);
            Assert.That(bag.Resolve("[guid]"), Is.TypeOf(typeof(Guid)));

            Assert.That(
                bag.Get<Var>(v => v.Null),
                Is.Null);

            Assert.That(
                bag.Get<Var>(v => v.Guid),
                Is.TypeOf(typeof(Guid)));
        }

        [Test]
        public void String()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            Assert.That(bag.Resolve("[string]"), Is.EqualTo(string.Empty));
            Assert.That(
                bag.Get<Var>(v => v.String),
                Is.EqualTo(string.Empty));
        }

        [Test]
        public void Date()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            Assert.That(bag.Resolve("[now]"), Is.TypeOf(typeof(DateTime)));
            Assert.That(bag.Resolve("[today]"), Is.EqualTo(DateTime.Today));
            Assert.That(bag.Resolve("[tomorrow]"), Is.EqualTo(DateTime.Today.AddDays(1)));
            Assert.That(bag.Resolve("[yesterday]"), Is.EqualTo(DateTime.Today.AddDays(-1)));

            Assert.That(bag.Get<Var>(v => v.Now), Is.TypeOf(typeof(DateTime)));
            Assert.That(bag.Get<Var>(v => v.Today), Is.EqualTo(DateTime.Today));
            Assert.That(bag.Get<Var>(v => v.Tomorrow), Is.EqualTo(DateTime.Today.AddDays(1)));
            Assert.That(bag.Get<Var>(v => v.Yesterday), Is.EqualTo(DateTime.Today.AddDays(-1)));
        }

        [Test]
        public void GetVariableNotFound()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            Assert.Throws<DataBagException>(() =>
            {
                bag.Get("no such variable");
            });
        }

        [Test]
        public void GetVariableAndConvert()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("number", "100");

            Assert.That(bag.Get<int>("number"), Is.EqualTo(100));
        }

        [Test]
        public void RegisterVariablesFromAssembly()
        {
            IDataBag bag = DataBagFactory.GetInstance();

            Assert.That(bag.IsRegistered(MyVar.MyTestVar), Is.False);

            bag.RegisterVariablesFromAssembly(typeof(DefaultVariables).Assembly);

            Assert.That(bag.Get(MyVar.MyTestVar), Is.EqualTo(MyVar.MyTestVar));
        }

        public class MyVar : IRegisterVariables
        {
            public const string MyTestVar = "myTestVar";

            public void Register(IDataBag dataBag)
            {
                dataBag.Register(MyTestVar, MyTestVar);
            }
        }
    }
}