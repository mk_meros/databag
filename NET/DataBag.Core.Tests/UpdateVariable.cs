﻿namespace DataBag.Core.Tests
{
    using NUnit.Framework;

    [TestFixture]
    public class UpdateVariable
    {
        [Test]
        public void RestoreVariable()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            dataBag.Register("myVar1", "var1Value1");
            dataBag.Register("myVar1", "var1Value2");
            dataBag.Register("myVar1", "var1Value3");

            Assert.That(dataBag.Resolve("[myVar1]"), Is.EqualTo("var1Value3"));

            dataBag.Restore("myVar1");
            Assert.That(dataBag.Resolve("[myVar1]"), Is.EqualTo("var1Value2"));

            dataBag.Restore("myVar1");
            Assert.That(dataBag.Resolve("[myVar1]"), Is.EqualTo("var1Value1"));
        }

        [Test]
        public void RestoreNotfinedVariable()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            Assert.Throws<DataBagException>(() =>
            {
                dataBag.Restore("myVar1");
            });
        }

        [Test]
        public void CannotRestoreIfVariableHasNotBeenUpdated()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            dataBag.Register("myVar1", "firstValue");
            Assert.Throws<DataBagException>(() =>
            {
                dataBag.Restore("myVar1");
            });
        }

        [Test]
        public void RestorePoint_NotCreated()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            dataBag.Register("var1", "var1Value");

            Assert.Throws<DataBagException>(() =>
            {
                dataBag.RestoreBackup("test");
            });
        }

        [Test]
        public void RestorePoint_VariableNotExists()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            dataBag.CreateBackup("test");
            dataBag.Register("var1", "var1Value");

            Assert.That(dataBag.Resolve("[var1]"), Is.EqualTo("var1Value"));

            dataBag.RestoreBackup("test");

            Assert.That(dataBag.Resolve("[var1]"), Is.EqualTo("[var1]"));
        }

        [Test]
        public void RestorePoint_VariableUpdated()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();

            dataBag.Register("var1", "var1Value");
            dataBag.CreateBackup("test");
            dataBag.Register("var1", "var1ValueUpdated");

            Assert.That(dataBag.Resolve("[var1]"), Is.EqualTo("var1ValueUpdated"));

            dataBag.RestoreBackup("test");

            Assert.That(dataBag.Resolve("[var1]"), Is.EqualTo("var1Value"));
        }

        [Test]
        public void RestorePointAndRestoreVariable()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();

            dataBag.Register("var1", "var1Value1");
            dataBag.Register("var1", "var1Value2");
            dataBag.Register("var1", "var1Value3");
            dataBag.CreateBackup("test");
            dataBag.Restore("var1");

            Assert.That(dataBag.Resolve("[var1]"), Is.EqualTo("var1Value2"));

            dataBag.RestoreBackup("test");

            Assert.That(dataBag.Resolve("[var1]"), Is.EqualTo("var1Value3"));
        }
    }
}
