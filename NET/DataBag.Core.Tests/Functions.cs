﻿namespace DataBag.Core.Tests
{
    using System;

    using NUnit.Framework;
    using NUnit.Framework.Internal;

    [TestFixture]
    public class Functions
    {
        [TestCase("[number:add(5)]", typeof(int), 15)]
        [TestCase("[number:add(-5)]", typeof(int), 5)]
        [TestCase("[text:toUpper()]", typeof(string), "TEST ")]
        [TestCase("[text:toLower()]", typeof(string), "test ")]
        [TestCase("[lowercase:firstUpper()]", typeof(string), "Lowercase")]
        [TestCase("[text:replace(Te,XX)]", typeof(string), "XXst ")]
        [TestCase("[text:trim()]", typeof(string), "Test")]
        [TestCase("[text:length()]", typeof(int), 5)]
        [TestCase("[forStringFormat:format(1,value)]", typeof(string), "test 1 and value")]
        [TestCase("[text:trim():length():add(10):add(-5)]", typeof(int), 9)]
        public void SingleVariableWithFunction(string expression, Type type, object result)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("number", 10);
            bag.Register("text", "Test ");
            bag.Register("lowercase", "lowercase");
            bag.Register("forStringFormat", "test {0} and {1}");

            var value = bag.Resolve(expression);
            Assert.That(value.GetType(), Is.EqualTo(type));
            Assert.That(value, Is.EqualTo(result));
        }

        [Test]
        public void FunctionMultipleVariablesWithoutConversion()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("number", 12);
            bag.Register("test", "today is [today:year():add(1)] and [number:add(2)]");
            var value = bag.Resolve("[test]");

            var year = System.DateTime.Today.Year + 1;
            Assert.That(value.GetType(), Is.EqualTo(typeof(string)));
            Assert.That(value, Is.EqualTo($"today is {year} and 14"));
        }

        [Test]
        public void FunctionSingleVariableAndConvertResult()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("text", "value");

            var result = bag.Resolve<int>("[text:isEqualTo(value2):then(10,5)]");
            Assert.That(result, Is.EqualTo(5));
        }

        [Test]
        public void FunctionMultipleVariablesAndConvertResult()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("text", "value");

            var result = bag.Resolve<double>("[text:isEqualTo(value2):then(10,5)].5");
            Assert.That(result, Is.EqualTo(5.5));
        }

        [Test]
        public void DateTime()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("date", new DateTime(2000, 1, 1));

            Assert.That(bag.Resolve("[date:addDays(1)]"), Is.EqualTo(new DateTime(2000, 1, 2)));
            Assert.That(bag.Resolve("[date:addMonths(1)]"), Is.EqualTo(new DateTime(2000, 2, 1)));
            Assert.That(bag.Resolve("[date:addYears(1)]"), Is.EqualTo(new DateTime(2001, 1, 1)));

            Assert.That(bag.Resolve("[date:day()]"), Is.EqualTo(1));
            Assert.That(bag.Resolve("[date:month()]"), Is.EqualTo(1));
            Assert.That(bag.Resolve("[date:year()]"), Is.EqualTo(2000));

            Assert.That(bag.Resolve("[date:toString(dd/MM/yyyy)]"), Is.EqualTo("01/01/2000"));
        }

        [TestCase("[null:add(4)]")]
        [TestCase("[null_wrapper:add(5)]")]
        [TestCase("[[null:add(5)] test")]
        public void CannotInvokeFunctionOnNull(string expression)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("null", null);
            bag.Register("null_wrapper", "[null]");

            Assert.Throws<DataBagException>(() =>
            {
                var value = bag.Resolve(expression);
            });
        }

        [TestCase("[myInt:add2(4)]")]
        [TestCase("[myString:add(5)]")]
        public void UnknownFunction(string expression)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("myInt", 1);
            bag.Register("myString", "1");

            Assert.Throws<DataBagException>(() =>
            {
                var value = bag.Resolve(expression);
            });
        }

        [TestCase("[myInt:add]")]
        [TestCase("[myInt:add 2]")]
        [TestCase("[myInt:add (2)]")]
        [TestCase("[myInt:]")]
        [TestCase("[myInt::]")]
        [TestCase("[myInt:add(2):]")]
        [TestCase("[myInt:add(2):a]")]
        public void IncorrectSyntax(string expression)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("myInt", 1);

            Assert.That(bag.Resolve(expression), Is.EqualTo(expression));
        }

        [TestCase("[var1:join(test)]", "var1Valuetest")]
        [TestCase("[var1:join(test)]_x", "var1Valuetest_x")]
        [TestCase("[var1:join(test):join(2)]", "var1Valuetest2")]
        [TestCase("[var1:join(test):join(2)]_x", "var1Valuetest2_x")]
        [TestCase("[var2:join(test)]", "var2Valuetest")]
        [TestCase("[var2:join(test)]_x", "var2Valuetest_x")]
        [TestCase("[var2:join(test):join(2)]", "var2Valuetest2")]
        [TestCase("[var2:join(test):join(2)]_x", "var2Valuetest2_x")]
        public void CustomFunction(string expression, string result)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("var1", "var1Value");
            bag.RegisterDynamic("var2", () => "var2Value");
            bag.DefineFunction<string>(
                "join", 
                (input, args) =>
                {
                    return input + args[0];
                });

            Assert.That(bag.Resolve(expression), Is.EqualTo(result));
        }

        [Test]
        public void ReplaceFunction()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("number", 100);
            bag.DefineFunction<int>(
                "add", 
                (input, args) =>
                {
                    return "replaced";
                });

            Assert.That(bag.Resolve("[number:add(5)]"), Is.EqualTo("replaced"));
        }

        [Test]
        public void CustomFunctionReturnsNull()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("text", "value");
            bag.DefineFunction<string>(
                "ToNull", 
                (input, args) =>
                {
                    return null;
                });

            Assert.That(bag.Resolve("[text:ToNull()]"), Is.EqualTo(null));
        }

        [Test]
        public void CustomFunctionMultipleReturnsNull()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("text", "value");
            bag.DefineFunction<string>(
                "ToNull", 
                (input, args) =>
                {
                    return null;
                });

            Assert.Throws<DataBagException>(() =>
            {
                bag.Resolve("[text:ToNull():Add(3)]");
            });
        }

        [Test]
        public void RepeatFunction()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("text", "myString");

            Assert.That(
                bag.Resolve("[text:repeat(2)]"),
                Is.EqualTo("myStringmyString"));

            Assert.That(
                bag.Resolve("[string:repeat(x,10)]"),
                Is.EqualTo("xxxxxxxxxx"));
        }

        [Test]
        public void UseVariablesInFunctionParamters()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("age", 100);
            bag.Register("p1", 1);
            bag.Register("p2", 2);

            Assert.That(
                bag.Resolve("[age:add([p1]):add(-[p2])]"),
                Is.EqualTo(99));
        }

        [Test]
        public void StringConditionalExpressions()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register(
                "hello",
                "Hello [gender:isEqualTo(male):then(Mr,Ms)] [name]!");

            bag.Register("name", "John");
            bag.Register("gender", "male");
            Assert.That(bag.Resolve("[hello]"), Is.EqualTo("Hello Mr John!"));

            bag.Register("name", "Emma");
            bag.Register("gender", "female");
            Assert.That(bag.Resolve("[hello]"), Is.EqualTo("Hello Ms Emma!"));
        }

        [Test]
        public void IntConditionalExpressions()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("age", 17);

            Assert.That(
                bag.Resolve("[age:isEqualTo(17):then(OK,Access Denied)]"),
                Is.EqualTo("OK"));

            Assert.That(
                bag.Resolve("[age:isGreaterThan(18):then(OK,Access Denied)]"),
                Is.EqualTo("Access Denied"));

            Assert.That(
                bag.Resolve("[age:isEqualToOrGreaterThan(18):then(OK,Access Denied)]"),
                Is.EqualTo("Access Denied"));

            Assert.That(
                bag.Resolve("[age:isLessThan(18):then(OK,Access Denied)]"),
                Is.EqualTo("OK"));

            Assert.That(
                bag.Resolve("[age:isEqualToOrLessThan(18):then(OK,Access Denied)]"),
                Is.EqualTo("OK"));
        }

        [Test]
        public void FunctionNoParameters()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("a", "varValue");

            bag.DefineFunction<string>(
                "fun", 
                (input, args) =>
                {
                    Assert.That(args.Length, Is.EqualTo(0));
                    return "OK";
                });

            var a = bag.Resolve("[a:fun()]");
            Assert.That(a, Is.EqualTo("OK"));
        }

        [TestCase("text", "text")]
        [TestCase("with space", "with space")]
        [TestCase("1", "1")]
        [TestCase(".", ".")]
        [TestCase("'text'", "'text'")]
        [TestCase("\"inQuotes\"", "inQuotes")]
        [TestCase("\"in Quotes with spaces\"", "in Quotes with spaces")]
        [TestCase("  should trim  ", "should trim")]
        [TestCase("\"comma in, quotes,\"", "comma in, quotes,")]
        public void FunctionSingleParameter(string parameter, string value)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("a", "varValue");
            bag.DefineFunction<string>(
                "fun", 
                (input, args) =>
                {
                    Assert.That(args.Length, Is.EqualTo(1));
                    Assert.That(args[0], Is.EqualTo(value));
                    return "OK";
                });

            var a = bag.Resolve($"[a:fun({parameter})]");
            Assert.That(a, Is.EqualTo("OK"));
        }

        [TestCase("1, 2", "1", "2")]
        [TestCase(",", "", "")]
        [TestCase("1,test", "1", "test")]
        [TestCase("1,\"test\"", "1", "test")]
        [TestCase("\"1.0\",\"test a\"", "1.0", "test a")]
        [TestCase("\"1,0\",\"test, a\"", "1,0", "test, a")]
        public void FunctionMultipleParameters(string parameters, string value1, string value2)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("a", "varValue");
            bag.DefineFunction<string>(
                "fun", 
                (input, args) =>
                {
                    Assert.That(args.Length, Is.EqualTo(2));
                    Assert.That(args[0], Is.EqualTo(value1));
                    Assert.That(args[1], Is.EqualTo(value2));
                    return "OK";
                });

            var a = bag.Resolve($"[a:fun({parameters})]");
            Assert.That(a, Is.EqualTo("OK"));
        }

        [Test]
        public void FunctionMultipleParametersExample()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("a", "varValue");
            bag.Register("b", "10");
            bag.DefineFunction<string>(
                "fun", 
                (input, args) =>
                {
                    Assert.That(args.Length, Is.EqualTo(6));
                    Assert.That(args[0], Is.EqualTo("-10"));
                    Assert.That(args[1], Is.EqualTo("2,0"));
                    Assert.That(args[2], Is.EqualTo("3"));
                    Assert.That(args[3], Is.EqualTo("4"));
                    Assert.That(args[4], Is.EqualTo("string"));
                    Assert.That(args[5], Is.EqualTo("comma, comma"));
                    return "OK";
                });

            var a = bag.Resolve($"[a:fun(-[b], \"2,0\",3 , \"4\",  string, \"comma, comma\")]");
            Assert.That(a, Is.EqualTo("OK"));
        }
    }
}
