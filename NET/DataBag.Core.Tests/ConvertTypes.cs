﻿namespace DataBag.Core.Tests
{
    using System;
    using NUnit.Framework;
    using NUnit.Framework.Internal;

    [TestFixture]
    public class ConvertTypes
    {
        private enum MyEnum
        {
            Zero = 0,
            Second = 2,
            First = 1
        }

        [TestCase(null, null)]
        [TestCase(null, typeof(string))]
        public void NullValue(object input, Type convertType)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            var a = bag.Resolve(input, convertType);
            Assert.That(a, Is.Null);
        }

        [Test]
        public void ConvertWithGenericNotation()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            var value = bag.Resolve<int>("22");
            var value2 = bag.Resolve<int>(22);
            Assert.That(value, Is.EqualTo(value2));
        }

        [TestCase(1, typeof(int), 1)]
        [TestCase("string", typeof(string), "string")]
        [TestCase(1, typeof(string), "1")]
        [TestCase("1", typeof(int), 1)]
        [TestCase(true, typeof(string), "True")]
        [TestCase("True", typeof(bool), true)]
        public void ConvertWithoutVariables(object input, Type convertType, object output)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            var value = bag.Resolve(input, convertType);
            Assert.That(value.GetType(), Is.EqualTo(convertType));
            Assert.That(value, Is.EqualTo(output));
        }

        [Test]
        public void ConvertStringToGuid()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            var guid = Guid.NewGuid();
            var value = bag.Resolve<Guid>(guid.ToString());
            Assert.That(value, Is.EqualTo(guid));
        }

        [TestCase("[str_1]", typeof(string), "1")]
        [TestCase("[str_1]", typeof(int), 1)]
        [TestCase("[int_2]", typeof(int), 2)]
        [TestCase("[int_2]", typeof(string), "2")]
        [TestCase("1[str_1]", typeof(int), 11)]
        [TestCase("1[str_1]", typeof(string), "11")]
        [TestCase("1[int_2]", typeof(int), 12)]
        public void ConvertWithOneVariable(object input, Type convertType, object output)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("str_1", "1");
            bag.Register("int_2", 2);
            var value = bag.Resolve(input, convertType);
            Assert.That(value.GetType(), Is.EqualTo(convertType));
            Assert.That(value, Is.EqualTo(output));
        }

        [TestCase("[str_1_wrapper]", typeof(string), "1")]
        [TestCase("[str_1_wrapper]", typeof(int), 1)]
        [TestCase("[int_2_wrapper]", typeof(int), 2)]
        [TestCase("[int_2_wrapper]", typeof(string), "2")]
        [TestCase("1[str_1_wrapper]", typeof(int), 11)]
        [TestCase("1[str_1_wrapper]", typeof(string), "11")]
        public void ConvertWithNestedVariable(object input, Type convertType, object output)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("str_1", "1");
            bag.Register("int_2", 2);
            bag.Register("str_1_wrapper", "[str_1]");
            bag.Register("int_2_wrapper", "[int_2]");
            var value = bag.Resolve(input, convertType);
            Assert.That(value.GetType(), Is.EqualTo(convertType));
            Assert.That(value, Is.EqualTo(output));
        }

        [TestCase("[str_1][str_1_wrapper]", typeof(string), "11")]
        [TestCase("[int_2][int_2_wrapper]", typeof(int), 22)]
        [TestCase("[int_2][int_2_wrapper][str_1][str_1_wrapper]", typeof(int), 2211)]
        [TestCase("[int_2][int_2_wrapper][str_1][str_1_wrapper]", typeof(string), "2211")]
        public void ConvertWithMultipleVariables(object input, Type convertType, object output)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("str_1", "1");
            bag.Register("int_2", 2);
            bag.Register("str_1_wrapper", "[str_1]");
            bag.Register("int_2_wrapper", "[int_2]");
            var value = bag.Resolve(input, convertType);
            Assert.That(value.GetType(), Is.EqualTo(convertType));
            Assert.That(value, Is.EqualTo(output));
        }

        [Test]
        public void ConvertWithMultipleVariablesExample()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("age", 30);
            bag.Register("name", "John");
            bag.Register("surname", "Smith");
            bag.Register("hello", "Hi [name] [surname]! You have [age] years!");

            var result = bag.Resolve("Text: [hello]");
            Assert.That(result, Is.EqualTo("Text: Hi John Smith! You have 30 years!"));
        }

        [Test]
        public void UnknownConversion()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            Assert.Throws<DataBagException>(
                () => bag.Resolve("test", typeof(Exception)));
        }

        [Test]
        public void InvalidConversion()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            Assert.Throws<FormatException>(
                () => bag.Resolve<int>("test"));
        }

        [Test]
        public void ReplaceConverter()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("p", 100);

            // Define custom type converter
            bag.DefineTypeConverter<int, string>((input) =>
            {
                return input + " converted";
            });

            Assert.That(bag.Resolve<string>("[p]"), Is.EqualTo("100 converted"));

            // Replaces type converter
            bag.DefineTypeConverter<int, string>((input) =>
            {
                return input + " replaced";
            });

            Assert.That(bag.Resolve<string>("[p]"), Is.EqualTo("100 replaced"));
        }

        [Test]
        public void ConvertEnum()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("enum2", MyEnum.Second);

            Assert.That(
                bag.Resolve<MyEnum>(2),
                Is.EqualTo(MyEnum.Second));

            Assert.That(
                bag.Resolve<MyEnum>("Second"),
                Is.EqualTo(MyEnum.Second));

            Assert.That(
                bag.Resolve<MyEnum>("second"),
                Is.EqualTo(MyEnum.Second));

            Assert.That(
                bag.Resolve<int>("[enum2]"),
                Is.EqualTo(2));

            Assert.That(
                bag.Resolve<string>("[enum2]"),
                Is.EqualTo("Second"));

            Assert.That(
                bag.Resolve<MyEnum>("2"),
                Is.EqualTo(MyEnum.Second));

            Assert.Throws<DataBagException>(() =>
            {
                bag.Resolve<MyEnum>("invalidValue");
            });
        }

        [Test]
        public void ConvertNullToNullable()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("null", null);

            var nullableGuid = bag.Get<Guid?>("null");
            Assert.That(nullableGuid.HasValue, Is.False);
        }

        [Test]
        public void ConvertNotNullToNullable()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("guid", Guid.NewGuid());

            var nullableGuid = bag.Get<Guid?>("guid");
            Assert.That(nullableGuid.HasValue, Is.True);
        }

        [Test]
        public void ConvertToNullableDifferentType()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            var guid = Guid.NewGuid();
            bag.Register("strGuid", guid.ToString());

            var nullableGuid = bag.Get<Guid?>("strGuid");
            Assert.That(nullableGuid.HasValue, Is.True);
            Assert.That(nullableGuid.Value, Is.EqualTo(guid));
        }

        [Test]
        public void ConvertNullableToOriginalType()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("nullableGuid", (Guid?)Guid.NewGuid());

            var guid = bag.Get<Guid>("nullableGuid");
            Assert.That(guid, Is.Not.EqualTo(Guid.Empty));
        }

        [Test]
        public void ConvertNullableNullToOriginalType()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("nullableNullGuid", (Guid?)null);

            Assert.Throws<DataBagException>(() =>
            {
                bag.Get<Guid>("nullableNullGuid");
            });
        }
    }
}
