﻿namespace DataBag.Core.Settings
{
    public interface ISettings
    {
        char FunctionSeparator { get; }

        char FunctionParameterSeparator { get; }

        string StartVariableMarker { get; }

        string EndVariableMarker { get; }

        string FunctionNameRegexPattern { get; }

        string VariableNameRegexPattern { get; }
    }
}
