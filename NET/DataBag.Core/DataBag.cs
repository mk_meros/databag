﻿namespace DataBag.Core
{
    using System;
    using System.Collections.Generic;

    using Expressions;
    using Functions;
    using Resolver;
    using Settings;
    using TypeConverters;
    using Variables;

    public class DataBag : IDataBag
    {
        private ISettings settings;
        private VariableManager variableManager;
        private FunctionManager functionManager;
        private TypeConverterManager typeConverterManager;
        private ExpressionParser expressionParser;
        private ExpressionResolver expressionResolver;

        public DataBag(ISettings settings)
        {
            this.settings = settings;
            typeConverterManager = new TypeConverterManager();
            variableManager = new VariableManager(settings);
            functionManager = new FunctionManager(settings);
            expressionParser = new ExpressionParser(settings);

            expressionResolver = new ExpressionResolver(
                variableManager,
                functionManager,
                typeConverterManager,
                expressionParser);
        }

        public IDataBag DefineTypeConverter<TInput, TOutput>(Func<TInput, TOutput> converter)
        {
            var typeConverter = GenericTypeConverter<TInput, TOutput>.Create(converter);
            typeConverterManager.Add(typeConverter);
            return this;
        }

        public IDataBag DefineFunction<TInput>(string name, Func<TInput, string[], object> func)
        {
            var function = GenericFunction<TInput>.Create(name, func);
            functionManager.Add(function);
            return this;
        }

        public IDataBag CreateBackup(string name)
        {
            variableManager.CreateBackup(name);
            return this;
        }

        public IDataBag RestoreBackup(string name)
        {
            variableManager.RestoreBackup(name);
            return this;
        }

        public IDataBag RemoveBackup(string name)
        {
            variableManager.RemoveBackup(name);
            return this;
        }

        public IDataBag Register(string name, object objOrExpression)
        {
            var valueProvider = new StaticValue(objOrExpression);
            variableManager.Add(name, valueProvider);
            return this;
        }

        public IDataBag RegisterDynamic(string name, Func<object> getObjOrExpressionFunc)
        {
            var valueProvider = new DynamicValue(getObjOrExpressionFunc);
            variableManager.Add(name, valueProvider);
            return this;
        }

        public IDataBag Restore(string name)
        {
            variableManager.Restore(name);
            return this;
        }

        public IDataBag Remove(string name)
        {
            variableManager.Remove(name);
            return this;
        }

        public bool IsRegistered(string name)
        {
            return variableManager.Exists(name);
        }

        public IEnumerable<string> GetNames()
        {
            return variableManager.GetNames();
        }

        public TOutput Get<TOutput>(string name)
        {
            var objOrExpression = GetValue(name);
            return Resolve<TOutput>(objOrExpression);
        }

        public object Get(string name)
        {
            var objOrExpression = GetValue(name);
            return Resolve(objOrExpression);
        }

        public object Get(string name, Type outputType)
        {
            var objOrExpression = GetValue(name);
            return Resolve(objOrExpression, outputType);
        }

        public object Resolve(object objOrExpression)
        {
            return Resolve(objOrExpression, null);
        }

        public TOutput Resolve<TOutput>(object objOrExpression)
        {
            return (TOutput)Resolve(objOrExpression, typeof(TOutput));
        }

        public object Resolve(object objOrExpression, Type outputType)
        {
            return expressionResolver.Resolve(objOrExpression, outputType, new List<string>());
        }

        public void Dispose()
        {
        }

        private object GetValue(string name)
        {
            if (!variableManager.Exists(name))
            {
                throw new DataBagException($"Cannot find variable '{name}'");
            }

            return variableManager.GetOrNull(name).GetValue();
        }
    }
}
