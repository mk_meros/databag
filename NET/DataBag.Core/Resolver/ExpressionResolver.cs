﻿namespace DataBag.Core.Resolver
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Expressions;
    using Functions;
    using TypeConverters;
    using Variables;

    public class ExpressionResolver
    {
        private readonly List<VariableExpression> noVariables = new List<VariableExpression>(0);

        private VariableManager variableManager;
        private FunctionManager functionManager;
        private TypeConverterManager typeConverterManager;
        private ExpressionParser expressionParser;

        public ExpressionResolver(
            VariableManager variableManager,
            FunctionManager functionManager,
            TypeConverterManager typeConverterManager,
            ExpressionParser expressionParser)
        {
            this.typeConverterManager = typeConverterManager;
            this.variableManager = variableManager;
            this.functionManager = functionManager;
            this.expressionParser = expressionParser;
        }

        public object Resolve(object objOrExpression, Type outputType, List<string> variablesBlackList)
        {
            var variableExpressions = GetVariableExpressions(objOrExpression, variablesBlackList);
            if (variableExpressions.Count == 0)
            {
                return typeConverterManager.Convert(objOrExpression, outputType);
            }
           
            if (IsSingleVariableExpression(variableExpressions, (string)objOrExpression))
            {
                return ResolveFromSingleVariable(variableExpressions[0], outputType, variablesBlackList);
            }

            return ResolveFromMultipleVariables((string)objOrExpression, variableExpressions, variablesBlackList, outputType);
        }

        private List<VariableExpression> GetVariableExpressions(
            object objOrExpression,
            List<string> variablesBlackList)
        {
            if (objOrExpression == null || !objOrExpression.GetType().Equals(typeof(string)))
            {
                return noVariables;
            }

            return expressionParser
                .GetVariables((string)objOrExpression)
                .Where(v => !variablesBlackList.Contains(v.Expression))
                .ToList();
        }

        private object ResolveFromMultipleVariables(
            string expression,
            List<VariableExpression> variableExpressions,
            List<string> variablesBlackList,
            Type outputType)
        {
            foreach (var variableExpression in variableExpressions)
            {
                var resolvedValue = (string)ResolveFromSingleVariable(variableExpression, typeof(string), variablesBlackList);
                expression = expression.Replace(variableExpression.Expression, resolvedValue);
            }

            return Resolve(expression, outputType, variablesBlackList);
        }

        private object ResolveFromSingleVariable(
            VariableExpression variableExpression,
            Type outputType,
            List<string> variablesBlackList)
        {
            var valueProvider = variableManager.GetOrNull(variableExpression.Name);
            if (valueProvider == null)
            {
                variablesBlackList.Add(variableExpression.Expression);
                return typeConverterManager.Convert(variableExpression.Expression, outputType);
            }

            var resolvedValue = Resolve(valueProvider.GetValue(), null, variablesBlackList);
            foreach (var functionExpression in variableExpression.Functions)
            {
                resolvedValue = ExecuteFunction(functionExpression, resolvedValue);
            }

            return typeConverterManager.Convert(resolvedValue, outputType);
        }

        private object ExecuteFunction(FunctionExpression functionExpression, object value)
        {
            if (value == null)
            {
                throw new DataBagException($"Cannot use function '{functionExpression.Name}' on null value')");
            }

            var function = functionManager.GetOrNull(functionExpression.Name, value.GetType());
            if (function == null)
            {
                throw new DataBagException($"Cannot find '{functionExpression.Name}' function for '{value.GetType()}' type");
            }

            return function.Execute(value, functionExpression.Parameters);
        }

        private bool IsSingleVariableExpression(
            List<VariableExpression> variableExpressions,
            string expression)
        {
            return variableExpressions.Count == 1 &&
                   variableExpressions[0].Expression == expression;
        }
    }
}
