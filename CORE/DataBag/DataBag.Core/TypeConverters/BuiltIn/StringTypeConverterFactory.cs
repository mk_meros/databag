﻿namespace DataBag.Core.TypeConverters.BuiltIn
{
    using System;
    using System.Globalization;

    public class StringTypeConverterFactory
    {
        public static int ToInt(string input)
        {
            return int.Parse(input);
        }
        public static uint ToUint(string input)
        {
            return uint.Parse(input);
        }

        public static float ToFloat(string input)
        {
            return float.Parse(input);
        }

        public static double ToDouble(string input)
        {
            return double.Parse(input, CultureInfo.InvariantCulture);
        }

        public static decimal ToDecimal(string input)
        {
            return decimal.Parse(input);
        }

        public static bool ToBool(string input)
        {
            return bool.Parse(input);
        }

        public static Guid ToGuid(string input)
        {
            return Guid.Parse(input);
        }
    }
}