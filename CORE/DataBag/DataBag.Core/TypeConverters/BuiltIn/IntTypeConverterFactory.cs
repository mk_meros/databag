﻿namespace DataBag.Core.TypeConverters.BuiltIn
{
    public class IntTypeConverterFactory
    {
        public static uint ToUint(int input)
        {
            return (uint)input;
        }

        public static float ToFloat(int input)
        {
            return (float)input;
        }

        public static double ToDouble(int input)
        {
            return (double)input;
        }
        public static decimal ToDecimal(int input)
        {
            return (decimal)input;
        }
    }
}