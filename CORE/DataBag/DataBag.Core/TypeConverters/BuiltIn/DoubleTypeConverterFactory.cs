﻿namespace DataBag.Core.TypeConverters.BuiltIn
{
    public class DoubleTypeConverterFactory
    {
        public static uint ToUint(double input)
        {
            return (uint)input;
        }

        public static int ToInt(double input)
        {
            return (int)input;
        }

        public static float ToFloat(double input)
        {
            return (float)input;
        }
        public static decimal ToDecimal(double input)
        {
            return (decimal)input;
        }
    }
}
