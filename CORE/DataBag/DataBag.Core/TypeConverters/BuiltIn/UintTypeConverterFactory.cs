﻿namespace DataBag.Core.TypeConverters.BuiltIn
{
    public class UintTypeConverterFactory
    {
        public static int ToInt(uint input)
        {
            return (int)input;
        }

        public static float ToFloat(uint input)
        {
            return (float)input;
        }

        public static double ToDouble(uint input)
        {
            return (double)input;
        }
        public static decimal ToDecimal(uint input)
        {
            return (decimal)input;
        }    
    }
}