﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBag.Core.TypeConverters.BuiltIn
{
    public class DecimalTypeConverterFactory
    {
        public static uint ToUint(decimal input)
        {
            return (uint)input;
        }

        public static int ToInt(decimal input)
        {
            return (int)input;
        }

        public static float ToFloat(decimal input)
        {
            return (float)input;
        }
        public static double ToDouble(decimal input)
        {
            return (double)input;
        }
    }
}
