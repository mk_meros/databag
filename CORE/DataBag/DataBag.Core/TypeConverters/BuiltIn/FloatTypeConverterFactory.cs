﻿namespace DataBag.Core.TypeConverters.BuiltIn
{
    public class FloatTypeConverterFactory
    {
        public static uint ToUint(float input)
        {
            return (uint)input;
        }

        public static int ToInt(float input)
        {
            return (int)input;
        }

        public static double ToDouble(float input)
        {
            return (double)input;
        }
        public static decimal ToDecimal(float input)
        {
            return (decimal)input;
        }
    }
}
