﻿namespace DataBag.Core.TypeConverters
{
    using System;

    public interface ITypeConverter
    {
        Type InputType { get; }

        Type OutputType { get; }

        object Convert(object input);
    }
}
