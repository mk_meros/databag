﻿namespace DataBag.Core.TypeConverters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TypeConverterManager
    {
        private List<ITypeConverter> typeConverters;

        public TypeConverterManager()
        {
            typeConverters = new List<ITypeConverter>();
        }

        public void Add(ITypeConverter typeConverter)
        {
            Validate(typeConverter);

            var oldTypeConverter = GetOrNull(typeConverter.InputType, typeConverter.OutputType);
            if (oldTypeConverter != null)
            {
                typeConverters.Remove(oldTypeConverter);
            }

            typeConverters.Add(typeConverter);
        }

        public ITypeConverter GetOrNull(Type input, Type output)
        {
            return typeConverters.FirstOrDefault(
                c => c.InputType.IsAssignableFrom(input) &&
                     c.OutputType.IsAssignableFrom(output));
        }

        public object Convert(object input, Type outputType)
        {
            if (outputType == null)
            {
                return input;
            }

            if (input == null)
            {
                if (IsValueExceptNullable(outputType))
                {
                    throw new DataBagException($"Cannot convert null to value type '{outputType.Name}'");
                }

                return input;
            }

            var inputType = input.GetType();
            if (outputType.IsAssignableFrom(inputType))
            {
                return input;
            }

            var typeConverter = GetOrNull(inputType, outputType);
            if (typeConverter != null)
            {
                return typeConverter.Convert(input);
            }

            return DefaultConversion(input, outputType);   
        }

        private object DefaultConversion(object input, Type outputType)
        {
            var inputType = input.GetType();
            var nullableType = Nullable.GetUnderlyingType(outputType);
            if (nullableType != null)
            {
                var nullableTypeObject = Convert(input, nullableType);
                return Activator.CreateInstance(outputType, nullableTypeObject);
            }

            if (inputType.IsEnum && outputType.Equals(typeof(int)))
            {
                return (int)input;
            }
            else if (outputType.IsEnum)
            {
                try
                {
                    return Enum.Parse(outputType, input.ToString(), true);
                }
                catch (Exception)
                {
                    throw new DataBagException($"Cannot convert value '{input}' to enum '{outputType.Name}'");
                }
            }

            if (outputType.Equals(typeof(string)))
            {
                return input.ToString();
            }

            throw new DataBagException($"Cannot convert '{inputType.Name}' to '{outputType.Name}'. Conversion not defined.");
        }

        private bool IsValueExceptNullable(Type type)
        {
            if (type.IsValueType)
            {
                var isNullableType = Nullable.GetUnderlyingType(type) != null;
                return !isNullableType;
            }

            return false;
        }

        private void Validate(ITypeConverter typeConverter)
        {
            if (typeConverter == null)
            {
                throw new DataBagException("Type converter cannot be null");
            }

            if (typeConverter.InputType == null)
            {
                throw new DataBagException("Input type cannot be null");
            }

            if (typeConverter.OutputType == null)
            {
                throw new DataBagException("Output type cannot be null");
            }

            if (typeConverter.InputType == typeConverter.OutputType)
            {
                throw new DataBagException("Output type cannot be equal to input type");
            }
        }
    }
}
