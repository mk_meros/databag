﻿namespace DataBag.Core.Functions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    using Settings;

    public class FunctionManager
    {
        private List<IFunction> functions;
        private ISettings settings;

        public FunctionManager(ISettings settings)
        {
            functions = new List<IFunction>();
            this.settings = settings;
        }

        public void Add(IFunction function)
        {
            Validate(function);

            var oldFunction = GetOrNull(function.Name, function.InputType);
            if (oldFunction != null)
            {
                functions.Remove(oldFunction);
            }

            functions.Add(function);
        }

        public IFunction GetOrNull(string name, Type type)
        {
            return functions.FirstOrDefault(
                f =>
                    f.Name.ToLower() == name.ToLower() &&
                    f.InputType.IsAssignableFrom(type));
        }

        private void Validate(IFunction function)
        {
            var name = function.Name;

            if (function == null)
            {
                throw new DataBagException("Function cannot be null");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new DataBagException("Function name cannot be empty");
            }

            if (!GetNameValidator().IsMatch(name))
            {
                throw new DataBagException("Incorrect function name");
            }

            if (name.Contains(settings.FunctionSeparator.ToString()))
            {
                throw new DataBagException($"Incorrect function name ('{settings.FunctionSeparator}' is no allowed)");
            }

            if (name.Contains(settings.FunctionParameterSeparator.ToString()))
            {
                throw new DataBagException($"Incorrect function name ('{settings.FunctionParameterSeparator}' is no allowed)");
            }

            if (name.Contains(settings.StartVariableMarker))
            {
                throw new DataBagException($"Incorrect function name ('{settings.StartVariableMarker}' is no allowed)");
            }

            if (name.Contains(settings.EndVariableMarker))
            {
                throw new DataBagException($"Invalid function name ('{settings.EndVariableMarker}' is no allowed)");
            }
        }

        private Regex GetNameValidator()
        {
            return new Regex($"^{settings.FunctionNameRegexPattern}$");
        }
    }
}
