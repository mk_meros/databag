﻿namespace DataBag.Core.Functions
{
    using System;

    public class GenericFunction<TypeInput> : IFunction
    {
        private Func<TypeInput, string[], object> function;

        private GenericFunction()
        {
        }

        public Type InputType { get; private set; }

        public string Name { get; private set; }

        public static GenericFunction<TInput> Create<TInput>(
            string name, Func<TInput, string[], object> func)
        {
            if (func == null)
            {
                throw new DataBagException("Function cannot be null");
            }

            return new GenericFunction<TInput>()
            {
                InputType = typeof(TInput),
                Name = name,
                function = func
            };
        }

        public object Execute(object input, params string[] parameters)
        {
            return function((TypeInput)input, parameters);
        }
    }
}
