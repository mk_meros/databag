﻿namespace DataBag.Core.Functions.BuiltIn
{
    using System;
    using System.Globalization;

    public class DateTimeFunctionFactory
    {
        public static object ToString(DateTime input, string[] parameters)
        {
            return input.ToString(parameters[0], DateTimeFormatInfo.InvariantInfo);
        }

        public static object AddDays(DateTime input, string[] parameters)
        {
            var value = int.Parse(parameters[0]);
            return input.AddDays(value);
        }

        public static object AddMonths(DateTime input, string[] parameters)
        {
            var value = int.Parse(parameters[0]);
            return input.AddMonths(value);
        }

        public static object AddYears(DateTime input, string[] parameters)
        {
            var value = int.Parse(parameters[0]);
            return input.AddYears(value);
        }

        public static object Day(DateTime input, string[] parameters)
        {
            return input.Day;
        }

        public static object Month(DateTime input, string[] parameters)
        {
            return input.Month;
        }

        public static object Year(DateTime input, string[] parameters)
        {
            return input.Year;
        }
    }
}
