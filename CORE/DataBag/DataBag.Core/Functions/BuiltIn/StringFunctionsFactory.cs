﻿namespace DataBag.Core.Functions.BuiltIn
{
    public class StringFunctionFactory
    {
        public static object ToLower(string input, string[] parameters)
        {
            return input.ToLower();
        }

        public static object ToUpper(string input, string[] parameters)
        {
            return input.ToUpper();
        }

        public static object FirstUpper(string input, string[] parameters)
        {
            var first = input[0].ToString().ToUpper();
            return first + input.Substring(1);
        }

        public static object Trim(string input, string[] parameters)
        {
            return input.Trim();
        }

        public static object Length(string input, string[] parameters)
        {
            return input.Length;
        }

        public static object Substring(string input, string[] parameters)
        {   
            var startIndex = int.Parse(parameters[0]);
            return input.Substring(startIndex);
        }


        public static object Repeat(string input, string[] parameters)
        {
            string value;
            int count;

            if (!string.IsNullOrEmpty(input))
            {
                value = input;
                count = int.Parse(parameters[0]);
            }
            else
            {
                value = parameters[0];
                count = int.Parse(parameters[1]);
            }

            var result = string.Empty;
            for (int i = 0; i < count; i++)
            {
                result += value;
            }

            return result;
        }

        public static object Format(string input, string[] parameters)
        {
            return string.Format(input, parameters);
        }

        public static object Replace(string input, string[] parameters)
        {
            return input.Replace(parameters[0], parameters[1]);
        }

        public static object IsEqualTo(string input, string[] parameters)
        {
            return input == parameters[0];
        }
    }
}