﻿namespace DataBag.Core
{
    public interface IRegisterVariables
    {
        void Register(IDataBag dataBag);
    }
}
