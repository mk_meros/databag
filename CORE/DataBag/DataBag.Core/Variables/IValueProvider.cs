﻿namespace DataBag.Core.Variables
{
    public interface IValueProvider
    {
        object GetValue();
    }
}
