﻿namespace DataBag.Core.Variables.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class VariableNameAttribute : Attribute
    {
        public VariableNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}
