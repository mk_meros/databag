﻿namespace DataBag.Core.Variables.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class SkipClassNameInVariablesAttribute : Attribute
    {
    }
}
