﻿namespace DataBag.Core.Variables
{
    public class StaticValue : IValueProvider
    {
        private object value;

        public StaticValue(object value)
        {
            this.value = value;
        }

        public object GetValue()
        {
            return value;
        }
    }
}
