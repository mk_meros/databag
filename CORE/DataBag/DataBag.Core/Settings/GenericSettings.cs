﻿namespace DataBag.Core.Settings
{
    public class GenericSettings : ISettings
    {
        public string StartVariableMarker { get; set; } = "{";

        public string EndVariableMarker { get; set; } = "}";

        public char FunctionSeparator { get; set; } = ':';

        public char FunctionParameterSeparator { get; set; } = ',';

        public string FunctionNameRegexPattern { get; set; } = @"[a-zA-Z0-9_\-]+";

        public string VariableNameRegexPattern { get; set; } = @"[a-zA-Z0-9_\-\. ]+";
    }
}