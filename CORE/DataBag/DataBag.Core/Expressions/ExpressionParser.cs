﻿namespace DataBag.Core.Expressions
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;

    using Settings;
    using TextFieldParserCore;

    public class ExpressionParser
    {
        private ISettings settings;

        public ExpressionParser(ISettings settings)
        {
            this.settings = settings;
        }

        public List<VariableExpression> GetVariables(string expression)
        {
            var variablesRegex = GetVariablesRegex();
            var functionRegex = GetFunctionRegex();

            var variables = new List<VariableExpression>();
            var possibleVariables = variablesRegex.Matches(expression);
            foreach (Match possibleVariable in possibleVariables)
            {
                var isCorrectVariableSyntax = true;
                var nameWithFunctions = possibleVariable
                    .Groups[1].Value.Split(settings.FunctionSeparator);

                var variable = new VariableExpression();
                variable.Name = nameWithFunctions[0];
                variable.Expression = possibleVariable.Groups[0].Value;

                for (int i = 1; i < nameWithFunctions.Length; i++)
                {
                    var matchFunction = functionRegex.Match(nameWithFunctions[i]);
                    if (!matchFunction.Success)
                    {
                        isCorrectVariableSyntax = false;
                        break;
                    }

                    var function = new FunctionExpression();
                    function.Name = matchFunction.Groups[1].Value;
                    function.Parameters = string.IsNullOrEmpty(matchFunction.Groups[2].Value)
                        ? new string[0]
                        : GetParameters(matchFunction.Groups[2].Value);

                    variable.Functions.Add(function);
                }

                if (isCorrectVariableSyntax)
                {
                    variables.Add(variable);
                }
            }

            return variables;
        }

        private Regex GetVariablesRegex()
        {
            var startVar = Regex.Escape(settings.StartVariableMarker);
            var endVar = Regex.Escape(settings.EndVariableMarker);
            var pattern = $"{startVar}([^{startVar}]+?){endVar}";
            return new Regex(pattern);
        }

        private Regex GetFunctionRegex()
        {
            var pattern = $"^({settings.FunctionNameRegexPattern})\\((.+)?\\)$";
            return new Regex(pattern);
        }

        private string[] GetParameters(string value)
        {
            var reader = new StringReader(value);
            var fieldParser = new TextFieldParser(reader);
            fieldParser.TextFieldType = FieldType.Delimited;
            fieldParser.Delimiters = new string[] { settings.FunctionParameterSeparator.ToString() };
            fieldParser.HasFieldsEnclosedInQuotes = true;
            fieldParser.TrimWhiteSpace = true;
            return fieldParser.ReadFields();
        }
    }
}
