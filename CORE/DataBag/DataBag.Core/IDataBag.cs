﻿namespace DataBag.Core
{
    using System;
    using System.Collections.Generic;

    public interface IDataBag : IDisposable
    {
        /// <summary>
        /// Register a variable with the specified name and value (which can be either an object or expression).
        /// You can also update the variable by registering new value for an existing name.
        /// In order to return to the previous value, use Restore() function.
        /// </summary>
        /// <returns>The data bag.</returns>
        /// <param name="name">Variable name (case insensitive).</param>
        /// <param name="objOrExpression">Object or expression.</param>
        IDataBag Register(string name, object objOrExpression);

        /// <summary>
        /// Register a variable with the specified name and function that calculates the value (object or expression) in the runtime.
        /// You can also update the variable by registering new value for an existing name.
        /// In order to return to the previous value, use Restore() function.
        /// </summary>
        /// <returns>The data bag.</returns>
        /// <param name="name">Variable name (case insensitive).</param>
        /// <param name="getObjOrExpressionFunc">Function that calculates the value (object or expression) in the runtime.</param>
        IDataBag RegisterDynamic(string name, Func<object> getObjOrExpressionFunc);

        /// <summary>
        /// Gets the names of registered variables.
        /// Note: variable name is case insensitive.
        /// </summary>
        /// <returns>The names of registered variables.</returns>
        IEnumerable<string> GetNames();

        /// <summary>
        /// Get the value of variable with the specified name and convert to the given type.
        /// If the variable is registered for a different type, it will be automatically converted to the specified Output Type.
        /// </summary>
        /// <returns>Variable value.</returns>
        /// <param name="name">Variable name (case insensitive).</param>
        /// <typeparam name="TOutput">Output type.</typeparam>
        TOutput Get<TOutput>(string name);

        /// <summary>
        /// Get the value of variable with the specified name.
        /// </summary>
        /// <returns>Variable value.</returns>
        /// <param name="name">Variable name (case insensitive).</param>
        object Get(string name);

        /// <summary>
        /// Get the value of variable with the specified name and convert to the given type.
        /// </summary>
        /// <returns>Variable value.</returns>
        /// <param name="name">Variable name (case insensitive).</param>
        /// <param name="outputType">The Output type.</param>
        object Get(string name, Type outputType);

        /// <summary>
        /// Resolve the specified object or expression and convert to the Output Type.
        /// </summary>
        /// <returns>Input object or resolved expression converted to the Output Type.</returns>
        /// <param name="objOrExpression">Object or expression.</param>
        /// <typeparam name="TOutput">The Output Type.</typeparam>
        TOutput Resolve<TOutput>(object objOrExpression);

        /// <summary>
        /// Resolve the specified object or expression.
        /// </summary>
        /// <returns>Input object or resolved expression.</returns>
        /// <param name="objOrExpression">Object or expression.</param>
        object Resolve(object objOrExpression);

        /// <summary>
        /// Resolve the specified object or expression and convert to the Output Type.
        /// </summary>
        /// <returns>Input object or resolved expression converted to the Output Type.</returns>
        /// <param name="objOrExpression">Object or expression.</param>
        /// <param name="outputType">The Output type.</param>
        object Resolve(object objOrExpression, Type outputType);

        /// <summary>
        /// Remove variable with the specified name.
        /// </summary>
        /// <returns>The data bag.</returns>
        /// <param name="name">Variable name (case insensitive).</param>
        IDataBag Remove(string name);

        /// <summary>
        /// Restore a previous value of the variable with the specified name.
        /// Value can be restored only if has been registered multiple times under the same name.
        /// </summary>
        /// <returns>The data bag.</returns>
        /// <param name="name">Variable name (case insensitive).</param>
        IDataBag Restore(string name);

        /// <summary>
        /// Checks whether a variable with the specified name is registered.
        /// </summary>
        /// <returns>Is variable registered.</returns>
        /// <param name="name">The variable name (case insensitive).</param>
        bool IsRegistered(string name);

        /// <summary>
        /// Create backup of current registered values.
        /// </summary>
        /// <returns>The data bag.</returns>
        /// <param name="name">Name of backup.</param>
        IDataBag CreateBackup(string name);

        /// <summary>
        /// Restores a previously created backup.
        /// </summary>
        /// <returns>The data bag.</returns>
        /// <param name="name">Name of backup.</param>
        IDataBag RestoreBackup(string name);

        /// <summary>
        /// Remove backup.
        /// </summary>
        /// <returns>The data bag.</returns>
        /// <param name="name">Name of backup.</param>
        IDataBag RemoveBackup(string name);

        /// <summary>
        /// Defines the type converter.
        /// </summary>
        /// <returns>The data bag.</returns>
        /// <param name="converter">Converter function.</param>
        /// <typeparam name="TInput">Input type.</typeparam>
        /// <typeparam name="TOutput">Output type.</typeparam>
        IDataBag DefineTypeConverter<TInput, TOutput>(Func<TInput, TOutput> converter);

        /// <summary>
        /// Defines the function.
        /// Functions can be used in expressions to execute additional operations on variables.
        /// </summary>
        /// <returns>The data bag.</returns>
        /// <param name="name">Function name.</param>
        /// <param name="func">Function definition.</param>
        /// <typeparam name="TInput">Input type.</typeparam>
        IDataBag DefineFunction<TInput>(string name, Func<TInput, string[], object> func);
    }
}
