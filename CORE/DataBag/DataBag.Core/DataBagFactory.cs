﻿namespace DataBag.Core
{
    using System;
    using System.Linq;
    using System.Reflection;

    using Functions.BuiltIn;
    using Settings;
    using TypeConverters.BuiltIn;

    public static class DataBagFactory
    {
        /// <summary>
        /// Get DataBag instance with default settings.
        /// </summary>
        /// <returns></returns>
        public static IDataBag GetInstance()
        {
            return GetInstance(new GenericSettings());
        }

        /// <summary>
        /// Get DataBag instance with custom settings.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static IDataBag GetInstance(ISettings settings)
        {
            return new DataBag(settings)
                .DefineDefaultTypeConverters()
                .DefineDefaultFunctions()
                .RegisterVariablesFromAssembly(typeof(DataBagFactory).Assembly);
        }

        /// <summary>
        /// Register variables defined by 'IRegisterVariables' interface in the given assembly.
        /// You can get the assembly object by using: typeof(TypeDefinedInAssembly).Assembly
        /// </summary>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="assembly">The assembly that contains variables defined by 'IRegisterVariables' interface.</param>
        /// <returns></returns>
        public static IDataBag RegisterVariablesFromAssembly(this IDataBag dataBag, Assembly assembly)
        {
            var types = assembly.ExportedTypes
                .Where(t => t.GetInterfaces().Contains(typeof(IRegisterVariables)) &&
                               t.GetConstructor(Type.EmptyTypes) != null);

            foreach (var type in types)
            {
                var registerVariablesObject = (IRegisterVariables)Activator.CreateInstance(type);
                registerVariablesObject.Register(dataBag);
            }

            return dataBag;
        }

        /// <summary>
        /// Register all enums defined in the given assebmly.
        /// You can get the assembly object by using: typeof(TypeDefinedInAssembly).Assembly
        /// </summary>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="assembly">The assembly that contains enums to be registered.</param>
        /// <returns></returns>
        public static IDataBag RegisterEnumsFromAssembly(this IDataBag dataBag, Assembly assembly)
        {
            foreach (var type in assembly.ExportedTypes.Where(t => t.IsEnum))
            {
                foreach (var value in type.GetEnumValues())
                {
                    var variableName = DataBagExtensions.GetVariableName(type.Name, value.ToString());
                    dataBag.Register(variableName, value);
                }
            }

            return dataBag;
        }

        private static IDataBag DefineDefaultTypeConverters(this IDataBag dataBag)
        {
            return dataBag
                // String
                .DefineTypeConverter<string, int>(StringTypeConverterFactory.ToInt)
                .DefineTypeConverter<string, uint>(StringTypeConverterFactory.ToUint)
                .DefineTypeConverter<string, float>(StringTypeConverterFactory.ToFloat)
                .DefineTypeConverter<string, double>(StringTypeConverterFactory.ToDouble)
                .DefineTypeConverter<string, decimal>(StringTypeConverterFactory.ToDecimal)
                .DefineTypeConverter<string, bool>(StringTypeConverterFactory.ToBool)
                .DefineTypeConverter<string, Guid>(StringTypeConverterFactory.ToGuid)
                // Int
                .DefineTypeConverter<int, uint>(IntTypeConverterFactory.ToUint)
                .DefineTypeConverter<int, float>(IntTypeConverterFactory.ToFloat)
                .DefineTypeConverter<int, double>(IntTypeConverterFactory.ToDouble)
                .DefineTypeConverter<int, decimal>(IntTypeConverterFactory.ToDecimal)
                // UInt
                .DefineTypeConverter<uint, int>(UintTypeConverterFactory.ToInt)
                .DefineTypeConverter<uint, float>(UintTypeConverterFactory.ToFloat)
                .DefineTypeConverter<uint, double>(UintTypeConverterFactory.ToDouble)
                .DefineTypeConverter<uint, decimal>(UintTypeConverterFactory.ToDecimal)
                // Float
                .DefineTypeConverter<float, uint>(FloatTypeConverterFactory.ToUint)
                .DefineTypeConverter<float, int>(FloatTypeConverterFactory.ToInt)
                .DefineTypeConverter<float, double>(FloatTypeConverterFactory.ToDouble)
                .DefineTypeConverter<float, decimal>(FloatTypeConverterFactory.ToDecimal)
                // Double
                .DefineTypeConverter<double, uint>(DoubleTypeConverterFactory.ToUint)
                .DefineTypeConverter<double, int>(DoubleTypeConverterFactory.ToInt)
                .DefineTypeConverter<double, float>(DoubleTypeConverterFactory.ToFloat)
                .DefineTypeConverter<double, decimal>(DoubleTypeConverterFactory.ToDecimal)
                // Decimal
                .DefineTypeConverter<decimal, uint>(DecimalTypeConverterFactory.ToUint)
                .DefineTypeConverter<decimal, int>(DecimalTypeConverterFactory.ToInt)
                .DefineTypeConverter<decimal, float>(DecimalTypeConverterFactory.ToFloat)
                .DefineTypeConverter<decimal, double>(DecimalTypeConverterFactory.ToDouble);
        }

        private static IDataBag DefineDefaultFunctions(this IDataBag dataBag)
        {
            return dataBag
                .DefineFunction<bool>(nameof(BoolFunctionsFactory.Then), BoolFunctionsFactory.Then)
                
                .DefineFunction<string>(nameof(StringFunctionFactory.Length), StringFunctionFactory.Length)
                .DefineFunction<string>(nameof(StringFunctionFactory.ToLower), StringFunctionFactory.ToLower)
                .DefineFunction<string>(nameof(StringFunctionFactory.ToUpper), StringFunctionFactory.ToUpper)
                .DefineFunction<string>(nameof(StringFunctionFactory.FirstUpper), StringFunctionFactory.FirstUpper)
                .DefineFunction<string>(nameof(StringFunctionFactory.Trim), StringFunctionFactory.Trim)
                .DefineFunction<string>(nameof(StringFunctionFactory.Repeat), StringFunctionFactory.Repeat)
                .DefineFunction<string>(nameof(StringFunctionFactory.Format), StringFunctionFactory.Format)
                .DefineFunction<string>(nameof(StringFunctionFactory.Replace), StringFunctionFactory.Replace)
                .DefineFunction<string>(nameof(StringFunctionFactory.IsEqualTo), StringFunctionFactory.IsEqualTo)
                .DefineFunction<string>(nameof(StringFunctionFactory.Substring), StringFunctionFactory.Substring)

                .DefineFunction<DateTime>(nameof(DateTimeFunctionFactory.Day), DateTimeFunctionFactory.Day)
                .DefineFunction<DateTime>(nameof(DateTimeFunctionFactory.Month), DateTimeFunctionFactory.Month)
                .DefineFunction<DateTime>(nameof(DateTimeFunctionFactory.Year), DateTimeFunctionFactory.Year)
                .DefineFunction<DateTime>(nameof(DateTimeFunctionFactory.AddDays), DateTimeFunctionFactory.AddDays)
                .DefineFunction<DateTime>(nameof(DateTimeFunctionFactory.AddMonths), DateTimeFunctionFactory.AddMonths)
                .DefineFunction<DateTime>(nameof(DateTimeFunctionFactory.AddYears), DateTimeFunctionFactory.AddYears)
                .DefineFunction<DateTime>(nameof(DateTimeFunctionFactory.ToString), DateTimeFunctionFactory.ToString)

                .DefineFunction<int>(nameof(IntFunctionFactory.Add), IntFunctionFactory.Add)
                .DefineFunction<int>(nameof(IntFunctionFactory.IsEqualTo), IntFunctionFactory.IsEqualTo)
                .DefineFunction<int>(nameof(IntFunctionFactory.IsGreaterThan), IntFunctionFactory.IsGreaterThan)
                .DefineFunction<int>(nameof(IntFunctionFactory.IsEqualToOrGreaterThan), IntFunctionFactory.IsEqualToOrGreaterThan)
                .DefineFunction<int>(nameof(IntFunctionFactory.IsLessThan), IntFunctionFactory.IsLessThan)
                .DefineFunction<int>(nameof(IntFunctionFactory.IsEqualToOrLessThan), IntFunctionFactory.IsEqualToOrLessThan);
        }
    }
}
