﻿namespace DataBag.Core
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Variables.Attributes;

    public static class DataBagExtensions
    {
        /// <summary>
        /// Separator of class name and property name.
        /// </summary>
        public const string ClassPropertySeparator = ".";

        /// <summary>
        /// Get DataBag instance dedicated for temporary changes (which will be reverted when disposing the object).
        /// USAGE: using (dataBag.ForTempChanges()) { /* temp changes */ }
        /// </summary>
        /// <param name="dataBag">The data bag.</param>
        /// <returns>The data bag.</returns>
        public static IDataBag ForTempChanges(
            this IDataBag dataBag)
        {
            return new DataBagDisposable(dataBag);
        }

        /// <summary>
        /// Checks whether a variable specified by the lambda expression is registered.
        /// </summary>
        /// <typeparam name="T">Type of containing class.</typeparam>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="property">The property (specified by the lambda expression).</param>
        /// <returns>Is variable registered.</returns>
        public static bool IsRegistered<T>(
            this IDataBag dataBag,
            Expression<Func<T, object>> property)
            where T : class, new()
        {
            var name = GetVariableName(property);
            return dataBag.IsRegistered(name);
        }

        /// <summary>
        /// Register a variable specified by the lambda expression.
        /// </summary>
        /// <typeparam name="T">Type of containing class.</typeparam>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="property">The property (specified by the lambda expression).</param>
        /// <param name="objOrExpression">Object or expression.</param>
        /// <returns>The data bag.</returns>
        public static IDataBag Register<T>(
            this IDataBag dataBag,
            Expression<Func<T, object>> property,
            object objOrExpression)
            where T : class, new()
        {
            var name = GetVariableName(property);
            dataBag.Register(name, objOrExpression);
            return dataBag;
        }

        /// <summary>
        /// Register a variable specified by the lambda expression which value is calculated by function during resolving.
        /// </summary>
        /// <typeparam name="T">Type of containing class.</typeparam>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="property">The property (specified by the lambda expression).</param>
        /// <param name="getObjOrExpressionFunc"></param>
        /// <returns>The data bag.</returns>
        public static IDataBag RegisterDynamic<T>(
            this IDataBag dataBag,
            Expression<Func<T, object>> property,
            Func<object> getObjOrExpressionFunc)
            where T : class, new()
        {
            var name = GetVariableName(property);
            dataBag.RegisterDynamic(name, getObjOrExpressionFunc);
            return dataBag;
        }

        /// <summary>
        /// Resolve and copy the value from one variable to another.
        /// </summary>
        /// <typeparam name="TSource">Type of containing class for the first variable.</typeparam>
        /// <typeparam name="TDestination">Type of containing class for the second variable.</typeparam>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="propertyFrom">The source property (specified by the lambda expression).</param>
        /// <param name="propertyDestination">The target property (specified by the lambda expression).</param>
        /// <returns>The data bag.</returns>
        public static IDataBag Copy<TSource, TDestination>(
            this IDataBag dataBag,
            Expression<Func<TSource, object>> propertyFrom,
            Expression<Func<TDestination, object>> propertyDestination)
            where TSource : class, new()
            where TDestination : class, new()
        {
            var value = dataBag.Get<TSource>(propertyFrom);
            dataBag.Register<TDestination>(propertyDestination, value);
            return dataBag;
        }

        /// <summary>
        /// Restore a previous value of the variable specified by the lambda expression.
        /// </summary>
        /// <typeparam name="T">Type of containing class.</typeparam>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="property">The property (specified by the lambda expression).</param>
        /// <returns>The data bag.</returns>
        public static IDataBag Restore<T>(
            this IDataBag dataBag,
            Expression<Func<T, object>> property)
            where T : class, new()
        {
            var name = GetVariableName(property);
            dataBag.Restore(name);
            return dataBag;
        }

        /// <summary>
        /// Get the value of variable specified by the lambda expression.
        /// </summary>
        /// <typeparam name="T">Type of containing class.</typeparam>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="property">The property (specified by the lambda expression).</param>
        /// <returns>The resolved value.</returns>
        public static object Get<T>(
            this IDataBag dataBag,
            Expression<Func<T, object>> property)
            where T : class, new()
        {
            var name = GetVariableName(property);
            return dataBag.Get(name);
        }

        /// <summary>
        /// Get the value of variable specified by the lambda expression and convert to the given type.
        /// </summary>
        /// <typeparam name="T">Type of containing class.</typeparam>
        /// <typeparam name="TOutput">Output type.</typeparam>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="property">The property (specified by the lambda expression).</param>
        /// <returns>The resolved and converted value.</returns>
        public static TOutput Get<T, TOutput>(
            this IDataBag dataBag,
            Expression<Func<T, object>> property)
            where T : class, new()
        {
            var name = GetVariableName(property);
            return dataBag.Get<TOutput>(name);
        }

        /// <summary>
        /// Get new instance of the given type with pre populated properties (based on registered variables).
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="dataBag">The data bag.</param>
        /// <returns>New object with pre populated properties.</returns>
        public static T GetFor<T>(this IDataBag dataBag)
            where T : class, new()
        {
            return (T)GetFor(dataBag, typeof(T));
        }

        /// <summary>
        /// Get new instance of the given type with pre populated properties (based on registered variables).
        /// </summary>
        /// <param name="dataBag">The data bag.</param>
        /// <param name="type">Type of object.</param>
        /// <returns>New object with pre populated properties.</returns>
        public static object GetFor(this IDataBag dataBag, Type type)
        {
            var returnObject = Activator.CreateInstance(type);

            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.CanRead && p.CanWrite)
                .Where(p => p.GetCustomAttribute<IgnoreWhenResolvingClassAttribute>() == null);

            foreach (var property in properties)
            {
                var variableName = GetVariableName(property, type);
                if (dataBag.IsRegistered(variableName))
                {
                    var variableValue = dataBag.Get(variableName, property.PropertyType);
                    property.SetValue(returnObject, variableValue, null);
                }
            }

            var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (var field in fields)
            {
                var variableName = GetVariableName(field, type);
                if (dataBag.IsRegistered(variableName))
                {
                    var variableValue = dataBag.Get(variableName, field.FieldType);
                    field.SetValue(returnObject, variableValue);
                }
            }

            return returnObject;
        }

        /// <summary>
        /// Get the variable name based on class and property names.
        /// </summary>
        /// <param name="className">The class name.</param>
        /// <param name="propertyName">The property name.</param>
        /// <returns>Variable name.</returns>
        public static string GetVariableName(
            string className,
            string propertyName)
        {
            return $"{className}{ClassPropertySeparator}{propertyName}";
        }

        private static string GetVariableName<T>(
            Expression<Func<T, object>> property)
            where T : class, new()
        {
            var variableName = string.Empty;

            if (property.Body is UnaryExpression)
            {
                var unaryExpression = (UnaryExpression)property.Body;
                if (unaryExpression.Operand is MemberExpression)
                {
                    var expression = (MemberExpression)unaryExpression.Operand;
                    variableName = GetVariableName(expression.Member, typeof(T));
                }
            }
            else if (property.Body is MemberExpression)
            {
                var expression = (MemberExpression)property.Body;
                variableName = GetVariableName(expression.Member, typeof(T));
            }

            if (string.IsNullOrEmpty(variableName))
            {
                throw new DataBagException("The lambda expression 'property' should point to a valid Property/Field");
            }

            return variableName;
        }

        private static string GetVariableName(MemberInfo memberInfo, Type type)
        {
            var isCorrectType = memberInfo is PropertyInfo || memberInfo is FieldInfo;
            if (!isCorrectType)
            {
                return string.Empty;
            }

            var nameAttribute = memberInfo.GetCustomAttribute<VariableNameAttribute>();
            if (nameAttribute != null)
            {
                return nameAttribute.Name;
            }

            var skipClassName = type.GetCustomAttribute<SkipClassNameInVariablesAttribute>();
            if (skipClassName != null)
            {
                return memberInfo.Name;
            }

            return GetVariableName(type.Name, memberInfo.Name);
        }
    }
}
