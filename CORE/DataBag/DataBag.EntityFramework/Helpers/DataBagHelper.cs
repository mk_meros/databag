﻿namespace DataBag.EntityFramework.Helpers
{
    using System.Reflection;
    using DataBag.Core;

    public static class DataBagHelper
    {
        public static object Get(
            this IDataBag dataBag, 
            PropertyInfo property)
        {
            var name = property.ToVariableName();
            return dataBag.Get(name, property.PropertyType);
        }

        public static bool IsRegistered(
            this IDataBag dataBag, 
            PropertyInfo property)
        {
            var name = property.ToVariableName();
            return dataBag.IsRegistered(name);
        }

        public static object Get(
            this IDataBag dataBag,
            CustomForeignKeyAttribute foreignKey)
        {
            var name = foreignKey.ToVariableName();
            return dataBag.Get(name, foreignKey.GetPropertyType());
        }

        public static bool IsRegistered(
            this IDataBag dataBag, 
            CustomForeignKeyAttribute foreignKey)
        {
            var name = foreignKey.ToVariableName();
            return dataBag.IsRegistered(name);
        }

        public static string ToVariableName(this CustomForeignKeyAttribute foreignKey)
        {
            return DataBagExtensions.GetVariableName(
                foreignKey.Class.Name,
                foreignKey.Name);
        }

        public static string ToVariableName(this PropertyInfo property)
        {
             return DataBagExtensions.GetVariableName(
                property.DeclaringType.Name,
                property.Name);
        }
    }
}
