﻿namespace DataBag.EntityFramework
{
    using System;

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CustomForeignKeyAttribute : Attribute
    {
        public CustomForeignKeyAttribute(Type classType, string propertyName)
        {
            Class = classType;
            Name = propertyName;
        }

        public Type Class { get; private set; }

        public string Name { get; private set; }
        
        public Type GetPropertyType()
        {
            return Class.GetProperty(Name).PropertyType;
        }
    }
}
