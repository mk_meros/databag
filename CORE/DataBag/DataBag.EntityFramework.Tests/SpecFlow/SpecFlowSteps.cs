﻿namespace DataBag.EntityFramework.Tests.SpecFlow
{
    using System.Linq;
    using System.Reflection;

    using DataBag.Core;
    using DataBag.EntityFramework.Tests.DataModel;
    using Microsoft.EntityFrameworkCore;
    using TechTalk.SpecFlow;

    [Binding]
    public class SpecFlowSteps
    {
        private Assembly assembly;
        private IDataBag dataBag;
        private BloggingContext dbContext;

        public SpecFlowSteps()
        {
            assembly = typeof(SpecFlowSteps).Assembly;
            dataBag = DataBagFactory.GetInstance().RegisterVariablesFromAssembly(assembly);

            dbContext = GetBloggingContext();
            dbContext.Database.ExecuteSqlRaw("DELETE FROM Posts");
            dbContext.Database.ExecuteSqlRaw("DELETE FROM Blogs");
        }

        [Given(@"(.+) exists")]
        public void GivenSingleRecordExists(string entity)
        {
            var type = assembly.GetTypes().First(t => t.Name.EndsWith(entity));
            dbContext.Add(dataBag, type);
        }

        [Given(@"(.+) exists")]
        public void GivenMultipleRecordsExists(string entity, Table customValues)
        {
            var type = assembly.GetTypes().First(t => t.Name.EndsWith(entity));
            using (dataBag.ForTempChanges())
            {
                foreach (var row in customValues.Rows)
                {
                    foreach (var field in row.Keys)
                    {
                        var variableName = DataBagExtensions.GetVariableName(type.Name, field);
                        dataBag.Register(variableName, row[field]);
                    }

                    dbContext.Add(dataBag, type);
                }
            }
        }

        [Then(@"'(.*)' is displayed")]
        public void ThenTextIsDisplayed(string text)
        {
            text = dataBag.Resolve<string>(text);
            //// TODO: add assertion
        }

        public BloggingContext GetBloggingContext()
        {
            var cn = @"data source=localhost;initial catalog=Blogging;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";
            return new BloggingContext(new DbContextOptionsBuilder<BloggingContext>().UseSqlServer(cn).Options);
        }
    }
}
