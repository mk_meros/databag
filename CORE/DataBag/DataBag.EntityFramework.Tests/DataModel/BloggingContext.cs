﻿namespace DataBag.EntityFramework.Tests.DataModel
{
    using Microsoft.EntityFrameworkCore;

    public partial class BloggingContext : DbContext
    {
        public BloggingContext(DbContextOptions<BloggingContext> options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }

        public virtual DbSet<Blog> Blogs { get; set; }

        public virtual DbSet<Post> Posts { get; set; }
    }
}
