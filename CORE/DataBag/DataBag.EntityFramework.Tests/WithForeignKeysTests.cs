﻿namespace DataBag.EntityFramework.Tests
{
    using System.Linq;

    using DataBag.Core;
    using DataBag.EntityFramework.Tests.DataModel;
    using Microsoft.EntityFrameworkCore;
    using NUnit.Framework;

    [TestFixture]
    public class WithForeignKeysTests : BaseTest
    {
        [Test]
        public void AddEntity()
        {
            var dataBag = GetDataBag();
            var context = GetBloggingContext();

            context.Add<Blog>(dataBag);
            var addedPost = context.Add<Post>(dataBag);

            Assert.That(addedPost, Is.Not.Null);
            Assert.That(addedPost.PostId, Is.GreaterThan(0));
            Assert.That(addedPost.Title, Is.EqualTo(dataBag.Get<Post>(p => p.Title)));
            Assert.That(addedPost.Content, Is.EqualTo(dataBag.Get<Post>(p => p.Content)));
            Assert.That(addedPost.BlogId, Is.GreaterThan(0));
            Assert.That(addedPost.Blog, Is.Not.Null);
        }

        [Test]
        public void AddEntitySaveAtEnd()
        {
            var dataBag = GetDataBag();
            var context = GetBloggingContext();

            var addedBlog = context.Add<Blog>(dataBag, false);
            context.SaveChanges();

            var addedPost = context.Add<Post>(dataBag, false);
            context.SaveChanges();

            Assert.That(addedPost, Is.Not.Null);
            Assert.That(addedPost.PostId, Is.GreaterThan(0));
            Assert.That(addedPost.Title, Is.EqualTo(dataBag.Get<Post>(p => p.Title)));
            Assert.That(addedPost.Content, Is.EqualTo(dataBag.Get<Post>(p => p.Content)));
            Assert.That(addedPost.BlogId, Is.GreaterThan(0));
            Assert.That(addedPost.Blog, Is.Not.Null);
        }

        [Test]
        public void AddEntityCannotFindForeignKey()
        {
            var dataBag = GetDataBag();
            var context = GetBloggingContext();

            Assert.Throws<DbUpdateException>(() =>
            {
                context.Add<Post>(dataBag);
            });         
        }

        [Test]
        public void AddEntityCannotFindForeignKeyWithCustomId()
        {
            var dataBag = GetDataBag();
            var context = GetBloggingContext();
            dataBag.Register<Post>(p => p.BlogId, -1);

            Assert.Throws<DbUpdateException>(() =>
            {
                context.Add<Post>(dataBag);
            });
        }

        [Test]
        public void AddMultipleRecords()
        {
            var dataBag = GetDataBag();
            var context = GetBloggingContext();

            using (dataBag.ForTempChanges())
            {
                for (int i = 1; i <= 10; i++)
                {
                    dataBag.Register<Blog>(b => b.Name, $"Name {i}");
                    dataBag.Register<Blog>(b => b.Url, $"URL {i}");
                    context.Add<Blog>(dataBag);

                    for (int j = 1; j <= 2; j++)
                    {
                        dataBag.Register<Post>(p => p.Title, $"Title {i} {j}");
                        dataBag.Register<Post>(p => p.Content, $"Content {i} {j}");
                        context.Add<Post>(dataBag);
                    }        
                }
            }

            Assert.That(context.Blogs.Count(), Is.EqualTo(10));
            Assert.That(context.Posts.Count(), Is.EqualTo(20));

            var blog = context.Blogs.First(b => b.Name == "Name 5");
            Assert.That(blog.Url, Is.EqualTo("URL 5"));
            Assert.That(blog.Posts.Count, Is.EqualTo(2));
            Assert.That(blog.Posts.First().Title, Is.EqualTo("Title 5 1"));
            Assert.That(blog.Posts.First().Content, Is.EqualTo("Content 5 1"));
        }
    }
}
