﻿namespace DataBag.EntityFramework.Tests.DataModelCustom
{
    using Microsoft.EntityFrameworkCore;

    public partial class PostContext : DbContext
    {
        public PostContext(DbContextOptions<PostContext> options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }

        public virtual DbSet<Post> Posts { get; set; }
    }
}
