﻿namespace DataBag.EntityFramework.Tests.DataModelCustom
{
    using Microsoft.EntityFrameworkCore;

    public partial class BlogContext : DbContext
    {
        public BlogContext(DbContextOptions<BlogContext> options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }

        public virtual DbSet<Blog> Blogs { get; set; }
    }
}
