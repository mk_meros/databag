﻿namespace DataBag.EntityFramework.Tests.DataModelCustom
{
    using System.ComponentModel.DataAnnotations;

    using DataBag.Core;
    using DataBag.Core.Variables.Attributes;
    using DataBag.EntityFramework;
    using DataBag.EntityFramework.Attributes;

    public partial class Blog : IRegisterVariables
    {
        [DBVariable]
        [IgnoreWhenResolvingClass]
        public int BlogId { get; set; }

        [FindBy]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Url { get; set; }

        public void Register(IDataBag dataBag)
        {
            dataBag.Register<Blog>(b => b.Name, "Blog name")
                   .Register<Blog>(b => b.Url, "http://blog.com");
        }
    }
}
