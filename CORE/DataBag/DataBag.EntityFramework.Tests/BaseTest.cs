﻿namespace DataBag.EntityFramework.Tests
{
    using DataBag.Core;
    using DataBag.EntityFramework.Tests.DataModel;
    using NUnit.Framework;
    using Microsoft.EntityFrameworkCore;

    public class BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            var context = GetBloggingContext();

            context.Database.ExecuteSqlRaw("DELETE FROM Posts");
            context.Database.ExecuteSqlRaw("DELETE FROM Blogs");
        }

        public IDataBag GetDataBag()
        {
            return DataBagFactory
                .GetInstance()
                .RegisterVariablesFromAssembly(typeof(NoForeignKeysTests).Assembly);
        }

        public BloggingContext GetBloggingContext()
        {
            var cn = @"data source=localhost;initial catalog=Blogging;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";
            return new BloggingContext(new DbContextOptionsBuilder<BloggingContext>().UseSqlServer(cn).Options);
        }
    }
}
