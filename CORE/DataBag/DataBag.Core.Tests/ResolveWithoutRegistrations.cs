﻿namespace DataBag.Core.Tests
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class ResolveWithoutRegistrations
    {
        [Test]
        public void Null()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            var a = bag.Resolve(null);
            Assert.That(a, Is.Null);
        }

        [TestCase(typeof(int), 12)]
        [TestCase(typeof(bool), false)]
        [TestCase(typeof(string), "")]
        [TestCase(typeof(string), "my expression")]
        [TestCase(typeof(string), "my expression {this is not variable}")]
        public void SimpleTypes(Type expectedType, object expression)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            var value = bag.Resolve(expression);
            Assert.That(value.GetType(), Is.EqualTo(expectedType));
            Assert.That(value, Is.EqualTo(expression));
        }
    }
}
