﻿namespace DataBag.Core.Tests
{
    using System;
    using Core.Settings;
    using NUnit.Framework;
    using NUnit.Framework.Internal;

    [TestFixture]
    public class Settings
    {
        [Test]
        public void VariableMarkers()
        {
            var customSettings = new GenericSettings()
            {
                StartVariableMarker = "${",
                EndVariableMarker = "}"
            };

            IDataBag dataBag = DataBagFactory.GetInstance(customSettings);
            dataBag.Register("myVar1", "value of var1");
            var value = dataBag.Resolve("Test ${myVar1} {myVar1} [myVar1]");
            Assert.That(value, Is.EqualTo("Test value of var1 {myVar1} [myVar1]"));
        }

        [Test]
        public void DoubleMarkers()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            dataBag.Register("var", 1);
            var value = dataBag.Resolve("{{var}} and {{{var}");
            Assert.That(value, Is.EqualTo("{1} and {{1"));
        }

        [Test]
        public void DoubleMarkersWithNestedVariables()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            dataBag.Register("var", "{var2}");
            dataBag.Register("var2", 33);

            var value = dataBag.Resolve("{{var}} and {{{var}");
            Assert.That(value, Is.EqualTo("{33} and {{33"));
        }

        [Test]
        public void DoubleMarkersWithNestedSingleVariable()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            dataBag.Register("var", "{var2}");
            dataBag.Register("var2", "test");
            dataBag.Register("test", "hidden value!");

            var value = dataBag.Resolve("{{var}}");
            Assert.That(value, Is.EqualTo("hidden value!"));
        }

        [Test]
        public void NestedVariableDefinition()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.RegisterDynamic("var1", () => "test");
            bag.RegisterDynamic("var2", () => 1);

            var value = bag.Resolve("{var{var2}}");
            Assert.That(value, Is.EqualTo("test"));
        }

        [Test]
        public void DynamicExpression()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("var1", "today is {my");
            bag.Register("var2", "Var");
            bag.Register("var3", "4}");
            bag.Register("myVar4", "{today}");

            var value = bag.Resolve("{var1}{var2}{var3}");
            Assert.That(value, Is.EqualTo($"today is {DateTime.Today}"));
        }

        [TestCase("")]
        [TestCase("{test")]
        [TestCase("}test")]
        [TestCase("{test}")]
        [TestCase(":")]
        [TestCase("var(")]
        [TestCase("var)")]
        public void VariableNameValidation(string name)
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            Assert.Throws<DataBagException>(
                () => dataBag.Register(name, "test"));
        }
    }
}
